<?php
$fieldset_width="450px";
$table="";
if (isset($_GET["table"]))	$table=$_GET["table"];

?>
<h3><center>Recherche d'informations</h3></center>
<form action="./" method="get">
<input type="hidden" name="infos" value="<?php echo $info;?>">
<div class="example" data-text="Recherche">
	<div class="grid">
	    <div class="row cells8">
	        <div class="cell">
	            <div class="input-control">Table</div>
	        </div>
	        <div class="cell colspan3">
	            <div class="input-control select full-size" style="position: absolute;margin-top: -4px">
					<select name="table" onChange="this.form.submit();" >
						<option value="" <?php if ($table == "") echo " selected";?> disabled>Choisissez une table</option>
						<optgroup label="Management">
							<option value="ndoc" <?php if ($table=="ndoc") echo "selected";?>>Documents groupe AQ</option>
							<option value="reunions" <?php if ($table=="reunions") echo "selected";?>>CR reunions</option>
							<option value="urhinfos" <?php if ($table=="urhinfos") echo "selected";?>>Divers UMRH</option>
						</optgroup>
						<optgroup label="Documentation">
							<option value="p_urhaqdoc" <?php if ($table=="p_urhaqdoc") echo "selected";?>>Interne</option>
							<option value="p_comm_valo" <?php if ($table=="p_comm_valo") echo "selected";?>>Communication / valorisation</option>
							<option value="q_corpus" <?php if ($table=="q_corpus") echo "selected";?>>Externe</option>
						</optgroup>
						<optgroup label="Ressources humaines">
							<option value="habilitations" <?php if ($table=="habilitations") echo "selected";?>>Habilitations</option>
							<option value="u2_formation" <?php if ($table=="u2_formation") echo "selected";?>>Formations</option>
						</optgroup>
						<optgroup label="R&eacute;glementation">
							<option value="locaux" <?php if ($table=="locaux") echo "selected";?>>Locaux</option>
						</optgroup>
						<optgroup label="R&eacute;alisations">
							<option value="demandes" <?php if ($table=="demandes") echo "selected";?>>Demandes</option>
							<option value="s_protocol" <?php if ($table=="s_protocol") echo "selected";?>>Protocoles</option>
							<!--<option value="clabo">Cahiers de labo</option>
							<option value="s_echantillons">Echantillons</option>-->
						</optgroup>
						<optgroup label="Mat&eacute;riel">
							<option value="a_mat" <?php if ($table=="a_mat") echo "selected";?>>Tout</option>
						</optgroup>
					</select>
				</div>
			</div>
		</div>
	</div>
</div>
</form><br>
<hr>
<?php
if (isset($_GET["table"]))
{

	$titre="";
	$table=$_GET["table"];
	switch ($table) {
		case "reunions":
			$titre="Note";
			break;
		case "habilitations":
			$titre="Habilitation";
			break;
		case "locaux":
			$titre="Designation";
			break;
		case "demandes":
			$titre="Fichier_demande";
			break;
		// case "clabo":
		// case "s_echantillons":
		case "a_mat":
			$titre="Nature,Marque,Modele";
			break;
		case "ndoc":
		case "urhinfos":
		case "p_urhaqdoc":
		case "p_comm_valo":
		case "q_corpus":
		case "u2_formation":
		case "s_protocol":
			$titre="Titre";
			break;

	}
	echo "<center>";
	if ($table=="ndoc") 			{$id="Idndoc";$infos="management";echo "<h1>Documents groupe AQ</h1>";}
	if ($table=="reunions") 		{$id="Id";$infos="management";echo "<h1>CR reunions</h1>";}
	if ($table=="urhinfos") 		{$id="Id";$infos="management";echo "<h1>Divers UMRH</h1>";}
	if ($table=="p_urhaqdoc") 		{$id="Id";$infos="documentation";echo "<h1>Interne</h1>";}
	if ($table=="p_comm_valo") 		{$id="Id";$infos="documentation";echo "<h1>Communication / valorisation</h1>";}
	if ($table=="q_corpus") 		{$id="Id";$infos="documentation";echo "<h1>Externe</h1>";}
	if ($table=="habilitations") 	{$id="Id";$infos="ressources";echo "<h1>Habilitations</h1>";}
	if ($table=="u2_formation") 	{$id="Id";$infos="ressources";echo "<h1>Formations</h1>";}
	if ($table=="locaux") 			{$id="Id";$infos="ressources";echo "<h1>Locaux</h1>";}
	if ($table=="demandes") 		{$id="Id";$infos="realisations";echo "<h1>Demandes</h1>";}
	if ($table=="s_protocol") 		{$id="Id";$infos="realisations";echo "<h1>Protocoles</h1>";}
	if ($table=="a_mat") 			{$id="IDmat";$infos="equipements";echo "<h1>Mat&eacute;riel</h1>";}
	echo "</center>";
	
	$first_letter="";
	$sql="select $titre,$id from $table order by $titre";
	$req=mysqli_query($idBase,$sql) or die (mysqli_error($idBase));
	while ($res=mysqli_fetch_row($req))
	{
		if (isset($res[3]))	$id=$res[3];
		else				$id=$res[1];
		$letter=substr($res[0],0,1);
		$letter=ucfirst($letter);
		if ($letter != $first_letter)
		{
			if ($first_letter!="") echo "</ul><br />";
			print "<ul><h1>$letter</h1>";
			$first_letter=$letter;
		}
		print "<li><a href='./index.php?infos=$infos&$infos=on&type=$table&Id=$id'>".$res[0]."</a></li>";
	}
	echo "</ul>";
}
?>

