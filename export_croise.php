<?php 
include ("configuration.php");

//Connexion � la base de donn�es
mysqli_connect(HOST,USER,MDP,DBNAME);

$explo=$_GET["explo"];
switch ($explo)
{
	default:
		$list_attr="SELECT year(Date) as Annee,Valide,count(*) as Nb from p_urhaqdoc";
		$conditions=" where Forme = 'Mode op�ratoire' and Obsolete='Non'";
		$conditionsbase=$conditions;
		$group=" group by Valide,year(Date) ORDER BY year(Date),Valide";
		break;
}

$sql_mess="$list_attr $conditions $group";
// echo $sql_mess;
$time=time();
$nom_fichier="extract$time.xls";
$req=mysqli_query($idBase,$sql_mess);
header ("Content-Type: application / MS-Excel"); 
header ("Content-Disposition: attachment; filename = $nom_fichier"); 
	
// si on a un resultat
if (mysqli_num_rows($req) != 0) 
{ 
	$tab=array();
	$entete=array();
	while($donnees_messages=mysqli_fetch_assoc($req)) // On lit les entr�es une � une gr�ce � une boucle
	{
			$Annee=$donnees_messages["Annee"];
			$Valide=$donnees_messages["Valide"];
			if (!isset($entete[$Valide]))	$entete[$Valide]=$Valide;
			$total=$donnees_messages["Nb"];
			$tab[$Annee][$Valide]=$total;
	}
	echo '<table id=tab border="1" align="center" cellpadding="2" cellspacing="2" style="font-size:12px;"><tr class="th1"><th>Annee</th>';
	foreach ($entete as $cle => $valeur)	
	{
		echo "<th>$valeur</th>";
	}
	echo "<th>Total</th></tr>";
	$lignetotal=array();
	foreach($tab as $cle=>$valeur)
	{
		echo "<tr><td class=td1>$cle</td>";
		$total=0;
		foreach ($entete as $key => $valeur)
		{

			if (isset($tab[$cle][$valeur]))	
			{
				echo "<td style='text-align:right;width:100px;'>".$tab[$cle][$valeur]."</td>";
				$total+=$tab[$cle][$valeur];
				if (isset($lignetotal[$key]))	$lignetotal[$key]+=$tab[$cle][$valeur];
				else 							$lignetotal[$key]=$tab[$cle][$valeur];
			}
			else 							echo "<td></td>";
		}
		echo "<td class=td2 style='text-align:right;width:100px;'>".$total."</td></tr>";
	}
	echo "<tr><td class=td1>Total</td>";
	$total=0;
	foreach ($lignetotal as $key => $valeur)		
	{
		echo "<td class=td2 style='text-align:right;width:100px;'>".$total."</td>";
		$total+=$valeur;
	}
		echo "<td class=td3 style='text-align:right;width:100px;'>".$total."</td>";
	echo "</table>";
} 
?>