<?php
	switch ($type) 
	{
		// reglementation
		case "t_elevage_ele":
			$conditions=" 1 ";
			$order=" order by ele_id desc";
			$list_attr="*";
			$id_attr="ele_id";
			$titre="Elevage";
			break;
		case "t_ethiqueexpeanimale_eea":
			$conditions=" 1 ";
			$order=" order by eea_id desc";
			$list_attr="*";
			$id_attr="eea_id";
			$titre=utf8_decode("Ethique et expérimentation animale");
			break;
		case "t_agrements_agr":
			$conditions=" 1 ";
			$order=" order by agr_id desc";
			$list_attr="*";
			$id_attr="agr_id";
			$titre=utf8_decode("Agréments des bâtiments");
			break;

		// SBEA
		case "t_groupesbea_grs":
			$conditions=" 1 ";
			$order=" order by grs_id desc";
			$list_attr="*";
			$id_attr="grs_id";
			$titre="Groupe SBEA";
			break;
		case "t_crreunionssbea_crs":
			$conditions=" 1 ";
			$order=" order by crs_id desc";
			$list_attr="*";
			$id_attr="Crs_id";
			$titre=utf8_decode("CR réunions SBEA");
			break;
		case "t_docrefreth_dre":
			$conditions=" 1 ";
			$order=" order by dre_id desc";
			$list_attr="*";
			$id_attr="dre_id";
			$titre=utf8_decode("Documents de référence éthique");
			break;

		// Charte sanitaire
		case "t_groupecs_gcs":
			$conditions=" 1 ";
			$order=" order by gcs_id desc";
			$list_attr="*";
			$id_attr="gcs_id";
			$titre="Groupe CS";
			break;
		case "t_suivics_tcs":
			$conditions=" 1 ";
			$order=" order by tcs_id desc";
			$list_attr="*";
			$id_attr="tcs_id";
			$titre=utf8_decode("Auto-évaluations CS");
			break;
		case "t_planactioncs_pls":
			$conditions=" 1 ";
			$order=" order by pls_id desc";
			$list_attr="*";
			$id_attr="pls_id";
			$titre="Plans d'action CS";
			break;
		case "t_crreunionscs_ccs":
			$conditions=" 1 ";
			$order=" order by ccs_id desc";
			$list_attr="*";
			$id_attr="ccs_id";
			$titre=utf8_decode("CR réunions CS");
			break;
		case "t_documentationcs_dcs":
			$conditions=" 1 ";
			$order=" order by dcs_id desc";
			$list_attr="*";
			$id_attr="dcs_id";
			$titre="Documentation CS";
			break;

		// Documentation AQ
		case "t_documentinterneobsolete_dio":
			$conditions=" (doi_obsolete < now() and doi_obsolete != '0000-00-00') ";
			$order=" order by doi_id desc";
			$list_attr="*";
			$id_attr="doi_id";
			$titre=utf8_decode("Documentation obsolète");
			break;
		// Documentation AQ
		case "t_documentinterne_doi1":
		case "t_documentinterne_doi2":
		case "t_documentinterne_doi3":
		case "t_documentinterne_doi7":
		case "t_documentinterne_doi5":
		case "t_documentinterne_doi6":
			$forme=substr($type,21,1);
			$type=substr($type,0,21);
			$resForme=mysqli_fetch_object(mysqli_query($idBase,"select for_libelle from tr_forme_for where for_id=$forme"));
			$order=" order by doi_id desc";
			$conditions=" doi_forme=$forme and (doi_obsolete >= now() or doi_obsolete = '0000-00-00') ";
			$list_attr="doi_id,doi_aq,doi_titre,doi_equipe,doi_forme,doi_commentaire";
			$id_attr="doi_id";
			$titre="Documentation en cours (".utf8_decode($resForme->for_libelle).")";
			break;
		case "t_documentinterne_doi":
			$order=" order by doi_id desc";
			$conditions=" (doi_obsolete >= now() or doi_obsolete = '0000-00-00') ";
			$list_attr="doi_id,doi_aq,doi_titre,doi_equipe,doi_forme,doi_commentaire";
			$id_attr="doi_id";
			$titre="Documentation en cours";
			break;
		case "t_documentexterne_doe":
			$conditions=" 1 ";
			$conditions=" 1 ";
			$order=" order by doe_id desc";
			$list_attr="*";
			$id_attr="doe_id";
			$titre="Documentation externe";
			break;

		// Documentation AQ
		case "t_habilitations_hab1":
			$type=substr($type,0,19);
			$conditions=" (hab_datedepart < now() and hab_datedepart != '0000-00-00')";
			$order=" order by hab_id desc";
			$list_attr="*";
			$id_attr="hab_id";
			$titre="Habilitations: Archives RH";
			break;
		case "t_habilitations_hab2":
			$type=substr($type,0,19);
			$conditions=" (hab_jusque > now() or hab_jusque='0000-00-00') and (hab_datedepart > now() or hab_datedepart = '0000-00-00')";
			$order=" order by hab_id desc";
			$list_attr="hab_id,hab_aq,hab_nomprenom,hab_equipe,hab_habilitation,hab_delivrele,hab_document";
			$id_attr="hab_id";
			$titre=utf8_decode("Habilitations: en cours de validité");
			break;
		case "t_habilitations_hab3":
			$type=substr($type,0,19);
			$conditions=" (hab_jusque < now() and hab_jusque != '0000-00-00' and (hab_datedepart > now() or hab_datedepart = '0000-00-00'))";
			$order=" order by hab_id desc";
			$list_attr="hab_id,hab_aq,hab_nomprenom,hab_equipe,hab_habilitation,hab_delivrele,hab_jusque,hab_document";
			$id_attr="hab_id";
			$titre=utf8_decode("Habilitations: avec validité dépassée");
			break;
		case "t_habilitations_hab":
			$conditions=" 1 ";
			$order=" order by hab_id desc";
			$list_attr="*";
			$id_attr="hab_id";
			$titre=utf8_decode("Habilitations spécifiques");
			break;
		case "t_feea_fee":
			$conditions=" 1 ";
			$order=" order by fee_id desc";
			$list_attr="*";
			$id_attr="fee_id";
			$titre=utf8_decode("Formations éthique et expérimentation animale");
			break;
		case "t_formationaq_faq":
			$conditions=" 1 ";
			$order=" order by faq_id desc";
			$list_attr="*";
			$id_attr="faq_id";
			$titre="Formations AQ";
			break;
		case "t_stagiaires_sta":
			$conditions=" 1 ";
			$order=" order by sta_id desc";
			$list_attr="*";
			$id_attr="sta_id";
			$titre="Stagiaires";
			break;

		// Prévention
		case "t_groupeprevention_grp":
			$conditions=" 1 ";
			$order=" order by grp_id desc";
			$list_attr="*";
			$id_attr="grp_id";
			$titre=utf8_decode("Groupe Prévention");
			break;
		case "t_suiviprevention_sup":
			$conditions=" 1 ";
			$order=" order by sup_id desc";
			$list_attr="*";
			$id_attr="sup_id";
			$titre=utf8_decode("Suivi Prévention");
			break;
		case "t_documentationprevention_dop":
			$conditions=" 1 ";
			$order=" order by dop_id desc";
			$list_attr="*";
			$id_attr="dop_id";
			$titre=utf8_decode("Documentation Prévention");
			break;

		// Management qualité
		case "t_referentielqualite_req":
			$conditions=" 1 ";
			$order=" order by req_id desc";
			$list_attr="*";
			$id_attr="req_id";
			$titre=utf8_decode("Référentiels Qualités");
			break;
		case "t_politiqueaq_paq":
			$conditions=" 1 ";
			$order=" order by paq_id desc";
			$list_attr="*";
			$id_attr="paq_id";
			$titre="Politique AQ";
			break;
		case "t_organisationaq_oaq":
			$conditions=" 1 ";
			$order=" order by oaq_id desc";
			$list_attr="*";
			$id_attr="oaq_id";
			$titre="Organisation AQ";
			break;
		case "t_groupeaq_gaq":
			$conditions=" 1 ";
			$order=" order by gaq_id desc";
			$list_attr="*";
			$id_attr="gaq_id";
			$titre="Groupe AQ";
			break;
		case "t_autoevalaq_aea":
			$conditions=" 1 ";
			$order=" order by aea_id desc";
			$list_attr="*";
			$id_attr="aea_id";
			$titre=utf8_decode("Suivi AQ: Auto-évaluations");
			break;
		case "t_planactionaq_pla":
			$conditions=" 1 ";
			$order=" order by pla_id desc";
			$list_attr="*";
			$id_attr="pla_id";
			$titre="Suivi AQ: Plans d'action";
			break;
		case "t_auditsaq_aua":
			$conditions=" 1 ";
			$order=" order by aua_id desc";
			$list_attr="*";
			$id_attr="aua_id";
			$titre="Suivi AQ: Audits";
			break;
		case "t_crreunionsaq_cra":
			$conditions=" 1 ";
			$order=" order by cra_id desc";
			$list_attr="*";
			$id_attr="cra_id";
			$titre=utf8_decode("CR réunions AQ");
			break;

		// Matériel
		case "t_materielreforme_mar":
			$type="t_materiel_mat";
			$conditions=" 1 and (mar_datereforme < now() and mar_datereforme!='0000-00-00')";
			$order=" order by mar_id desc";
			$list_attr="mar_id,mar_aq,mar_site,mar_equipe,mar_localisation,mar_nature,mar_marque,mar_modele,mar_photo,mar_descriptif,mar_conforme,mar_datereforme";
			$id_attr="mar_id";
			$titre=utf8_decode("Equipements réformés");
			break;
		case "t_materielservice_mas":
			$type="t_materiel_mat";
			$conditions=" 1 and (mar_datereforme >= now() or mar_datereforme='0000-00-00')";
			$order=" order by mar_id desc";
			$list_attr="mar_id,mar_aq,mar_site,mar_equipe,mar_localisation,mar_nature,mar_marque,mar_modele,mar_photo,mar_descriptif,mar_conforme,mar_datereforme";
			$id_attr="mar_id";
			$titre="Equipements";
			break;
		case "t_materiel_mat":
			$type="t_materiel_mat";
			$conditions=" 1";
			$order=" order by mar_id desc";
			$list_attr="mar_id,mar_aq,mar_site,mar_equipe,mar_localisation,mar_nature,mar_marque,mar_modele,mar_photo,mar_descriptif,mar_conforme";
			$id_attr="mar_id";
			$titre="Equipements en service";
			break;

		// Expérimentations
		case "t_protocoles_pro":
			$conditions=" 1 ";
			$order=" order by pro_id desc";
			$list_attr="*";
			$id_attr="pro_id";
			$titre="Suivi des protocoles";
			break;
		case "t_cahierlabo_cla":
			$conditions=" 1 ";
			$order=" order by cla_id desc";
			$list_attr="*";
			$id_attr="cla_id";
			$titre="Cahiers de laboratoire";
			break;
		case "t_echantillons_ech":
			$conditions=" 1 ";
			$order=" order by ech_id desc";
			$list_attr="*";
			$id_attr="ech_id";
			$titre="Echantillons";
			break;

		// Communication
		case "t_communicationinterne_coi":
			$conditions=" 1 ";
			$order=" order by coi_id desc";
			$list_attr="*";
			$id_attr="coi_id";
			$titre="Communication interne";
			break;
		case "t_communicationexterne_coe":
			$conditions=" 1 ";
			$order=" order by coe_id desc";
			$list_attr="*";
			$id_attr="coe_id";
			$titre="Communication externe";
			break;

		// autres
		default:
			$conditions=" 1 ";
			$order="";
			$list_attr="*";
			$id_attr="";
			$titre="Table $type";
			break;
	}
?>