<div class="app-bar fixed-top" data-role="appbar">
<?php
if (isset($_SESSION["user"]))
{
?>
    <a class="app-bar-element" style="background-color:darkred" href="index.php"><span class="mif-home"></span>&nbsp;</a>
    <span class="app-bar-divider"></span>
    <a class="app-bar-element" href="#" onclick="specialClick()">Menu</a>
    <span class="app-bar-divider"></span>
    <a class="app-bar-element" href="./index.php?info=recherche">Recherche</a>
        
    <div class="app-bar-element place-right">
        <span class="dropdown-toggle"><span class="mif-cog"></span><?php echo " ",$_SESSION["user"];?></span>
        <div class="app-bar-drop-container padding10 place-right no-margin-top block-shadow fg-dark" data-role="dropdown" data-no-close="true" style="width: 220px">
            <ul class="unstyled-list fg-dark">
                <li><a href="./?connexion=on" class="fg-white3 fg-hover-yellow"><span class="fg-red mif-exit icon"></span> Se déconnecter</a></li>
            </ul>
        </div>
    </div>
<?php
}
elseif (defined('NOM_ONGLET_CONNEXION'))
{
?>
    <a class="app-bar-element" style="background-color:darkred" href="./index.php?connexion=on">Connexion</a>
    <?php
}

?>                	
</div>

<div data-role="charm" id="menu-special" data-position="left" onclick="specialClick()">
    <h1 class="text-light">Menu</h1>
    <ul class="vertical-menu">
        <li style="color:white;size:12px">
            <a href="#" class="dropdown-toggle">Réglementation</a>
            <ul class="d-menu" data-role="dropdown">
                <li><a href="index.php?info=t_elevage_ele">Elevage</a></li>
                <li><a href="index.php?info=t_ethiqueexpeanimale_eea">Ethique et expérimentation animale</a></li>
                <li><a href="index.php?info=t_agrements_agr">Agréments des bâtiments</a></li>
            </ul>
        </li>
        <li style="color:white">
            <a href="#" class="dropdown-toggle">SBEA</a>
            <ul class="d-menu" data-role="dropdown">
                <li><a href="index.php?info=t_groupesbea_grs">Groupe SBEA</a></li>
                <li><a href="index.php?info=t_crreunionssbea_crs">CR réunions SBEA</a></li>
                <li><a href="index.php?info=t_docrefreth_dre">Documents de référence éthique</a></li>
            </ul>
        </li>
        <li style="color:white">
            <a href="#" class="dropdown-toggle">Charte Sanitaire</a>
            <ul class="d-menu" data-role="dropdown">
                <li><a href="index.php?info=t_groupecs_gcs">Groupe CS</a></li>
                <li>
                    <a href="#" class="dropdown-toggle">Suivi CS</a>
                    <ul class="d-menu" data-role="dropdown">
                        <li><a href="index.php?info=t_suivics_tcs">Auto-évaluations</a></li>
                        <li><a href="index.php?info=t_planactioncs_pls">Plans d'action</a></li>
                    </ul>
                </li>
                <li><a href="index.php?info=t_crreunionscs_ccs">CR réunions CS</a></li>
                <li><a href="index.php?info=t_documentationcs_dcs">Documentation CS</a></li>
            </ul>
        </li>
        <li style="color:white">
            <a href="#" class="dropdown-toggle">Documentation AQ</a>
            <ul class="d-menu" data-role="dropdown">
                <li>
                    <a href="#" class="dropdown-toggle">Interne</a>
                    <ul class="d-menu" data-role="dropdown">
                        <li><a href="index.php?info=t_documentinterneobsolete_dio">Documents obsolètes</a></li>
                        <li>
                            <a href="index.php?info=t_documentinterne_doi" class="dropdown-toggle">En cours</a>
                            <ul class="d-menu" data-role="dropdown">
                                <li><a href="index.php?info=t_documentinterne_doi1">Trames types vierges</a></li>
                                <li><a href="index.php?info=t_documentinterne_doi2">Procédures</a></li>
                                <li><a href="index.php?info=t_documentinterne_doi3">Instructions</a></li>
                                <li><a href="index.php?info=t_documentinterne_doi7">Modes opératoires</a></li>
                                <li><a href="index.php?info=t_documentinterne_doi5">Enregistrements</a></li>
                                <li><a href="index.php?info=t_documentinterne_doi6">Programmes informatiques</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="index.php?info=t_documentexterne_doe">Externe</a></li>
            </ul>
        </li>
        <li style="color:white">
            <a href="#" class="dropdown-toggle">Ressources Humaines</a>
            <ul class="d-menu" data-role="dropdown">
                <li><a href="index.php?info=t_habilitations_hab1">Archives RH</a></li>
                <li>
                    <a href="index.php?info=t_habilitations_hab" class="dropdown-toggle">Habilitations spécifiques</a>
                    <ul class="d-menu" data-role="dropdown">
                        <li><a href="index.php?info=t_habilitations_hab2">En cours de validité</a></li>
                        <li><a href="index.php?info=t_habilitations_hab3">Validité dépassée</a></li>
                    </ul>
                </li>
                <li><a href="index.php?info=t_feea_fee">Formations éthique et expérimentation animale</a></li>
                <li><a href="index.php?info=t_formationaq_faq">Formations AQ</a></li>
                <li><a href="index.php?info=t_stagiaires_sta">Stagiaires</a></li>
            </ul>
        </li>
        <li style="color:white">
            <a href="#" class="dropdown-toggle">Prévention</a>
            <ul class="d-menu" data-role="dropdown">
                <li><a href="index.php?info=t_groupeprevention_grp">Groupe Prévention</a></li>
                <li><a href="index.php?info=t_suiviprevention_sup">Suivi Prévention</a></li>
                <li><a href="index.php?info=t_documentationprevention_dop">Documentation Prévention</a></li>
            </ul>
        </li>
        <li style="color:white">
            <a href="#" class="dropdown-toggle">Management Qualité</a>
            <ul class="d-menu" data-role="dropdown">
                <li><a href="index.php?info=t_referentielqualite_req">Référentiels Qualités</a></li>
                <li>
                    <a href="#" class="dropdown-toggle">AQ Herbipôle</a>
                    <ul class="d-menu" data-role="dropdown">
                        <li><a href="index.php?info=t_politiqueaq_paq">Politique AQ</a></li>
                        <li><a href="index.php?info=t_organisationaq_oaq">Organisation AQ</a></li>
                        <li><a href="index.php?info=t_groupeaq_gaq">Groupe AQ</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="dropdown-toggle">Suivi AQ</a>
                    <ul class="d-menu" data-role="dropdown">
                        <li><a href="index.php?info=t_autoevalaq_aea">Auto-évaluations</a></li>
                        <li><a href="index.php?info=t_planactionaq_pla">Plans d'action</a></li>
                        <li><a href="index.php?info=t_auditsaq_aua">Audits</a></li>
                    </ul>
                </li>
                <li><a href="index.php?info=t_crreunionsaq_cra">CR réunions AQ</a></li>
            </ul>
        </li>
        <li style="color:white">
            <a href="#" class="dropdown-toggle">Matériel</a>
            <ul class="d-menu" data-role="dropdown">
                <li><a href="index.php?info=t_materielreforme_mar">Equipements réformés</a></li>
                <li><a href="index.php?info=t_materielservice_mas">Equipements en service</a></li>
            </ul>
        </li>
        <li style="color:white">
            <a href="#" class="dropdown-toggle">Expérimentations</a>
            <ul class="d-menu" data-role="dropdown">
                <li><a href="index.php?info=t_protocoles_pro">Suivi des protocoles</a></li>
                <li><a href="index.php?info=t_cahierlabo_cla">Cahiers de laboratoire</a></li>
                <li><a href="index.php?info=t_echantillons_ech">Echantillons</a></li>
            </ul>
        </li>
        <li style="color:white">
            <a href="#" class="dropdown-toggle">Communication</a>
            <ul class="d-menu" data-role="dropdown">
                <li><a href="index.php?info=t_communicationinterne_coi">Interne</a></li>
                <li><a href="index.php?info=t_communicationexterne_coe">Externe</a></li>
            </ul>
        </li>
    </ul>
</div>
<script>
function specialClick(){
    var  charm = $("#menu-special").data("charm");
    if (charm.element.data("opened") === true) {
        charm.close();
    } else {
        charm.open();
    }
}
</script>
<script src="js/imageMapResizer.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('map').imageMapResize();
            });
        </script>