<?php
function createInputText($attr,$label,$resultat="") {
?>
    <!-- <div class="row cells8"> -->
        <div class="cell">
            <div class="input-control"><?php echo $label;?></div>
        </div>
        <div class="cell colspan3">
            <div class="input-control text full-size" style="position: absolute;margin-top: -4px">
                <input type="text" name="<?php echo $attr;?>" value="<?php echo $resultat;?>">
            </div>
        </div>
    <!-- </div> -->
<?php
}

function createInputTextArea($attr,$label,$resultat="") {
?>
    <!-- <div class="row cells8"> -->
        <div class="cell">
            <div class="input-control"><?php echo $label;?></div>
        </div>
        <div class="cell colspan3">
            <div class="input-control text full-size" style="position: absolute;margin-top: -4px">
                <textarea name="<?php echo $attr;?>"><?php echo $resultat;?></textarea>
            </div>
        </div>
    <!-- </div><br /><br /><br /><br /> -->
<?php
}

function createInputDate($attr,$label,$resultat="") {
?>
    <!-- <div class="row cells8"> -->
        <div class="cell colspan1">
            <div class="input-control"><?php echo $label;?></div>
        </div>
        <div class="cell colspan3">
            <div class="input-control text" data-role="datepicker" style="position: absolute;margin-top: -4px" data-format='yyyy-mm-dd'>
                <input type="text" name="<?php echo $attr;?>" value=<?php echo $resultat?>>
                <button class="button"><span class="mif-calendar"></span></button>
            </div>
        </div>
    <!-- </div><br /> -->
<?php
}

function createInputNumber($attr,$label,$resultat="") {
?>
    <!-- <div class="row cells8"> -->
        <div class="cell">
            <div class="input-control"><?php echo $label;?></div>
        </div>
        <div class="cell colspan3">
            <div class="input-control text full-size" style="position: absolute;margin-top: -4px">
                <input type="number" name="<?php echo $attr;?>" value="<?php echo $resultat;?>">
            </div>
        </div>
    <!-- </div><br /> -->
<?php
}

function createInputSelect($mysql,$id,$nomtable,$attr,$label,$liste=array()) { //Fonction permettant d'afficher une liste déroulante: il faut passer en paramètres la variable de connexion à la base de données, l'id, le nom et l'attribut à afficher de la table ainsi que le libelle à afficher à côté de l'input
?>
    <!-- <div class="row cells8" style="height: 20px"> -->
        <div class="cell">
            <div class="input-control"><?php echo $label;?></div>
        </div>
        <div class="cell colspan3">
            <div class="input-control select full-size" style="position: absolute;margin-top: -4px">
                <select name="<?php echo $attr;?>">
                    <option value="$id" disabled selected>Sélectionner un(e) <?php echo $label;?></option>
                    <?php
                    $req=mysqli_query($mysql,"SELECT DISTINCT $id,$attr FROM $nomtable ORDER BY $attr");
                    while ($res=mysqli_fetch_object($req))  
                    {
                        echo "<option value=".$res->$id;
                        if (in_array($res->$id, $liste)) echo " selected";
                        echo ">".$res->$attr."</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
    <!-- </div> -->
<?php
}

function createInputSelectWithoutLabel($mysql,$id,$nomtable,$attr,$label,$liste=array()) { //Fonction permettant d'afficher une liste déroulante: il faut passer en paramètres la variable de connexion à la base de données, l'id, le nom et l'attribut à afficher de la table ainsi que le libelle à afficher à côté de l'input
?>
    <div class="cell colspan3">
        <div class="input-control select full-size" style="position: absolute;margin-top: -4px">
            <select name="<?php echo $attr;?>">
                <option value="$id" disabled selected>Sélectionner un(e) <?php echo $label;?></option>
                <?php
                $req=mysqli_query($mysql,"SELECT DISTINCT $id,$attr FROM $nomtable ORDER BY $attr");
                while ($res=mysqli_fetch_object($req))  
                {
                    echo "<option value=".$res->$id;
                    if (in_array($res->$id, $liste)) echo " selected";
                    echo ">".$res->$attr."</option>";
                }
                ?>
            </select>
        </div>
    </div>
<?php
}

function createInputPassword($attr,$label) {
?>
    <!-- <div class="row cells7"> -->
        <div class="cell">
            <div class="input-control"><?php echo $label;?></div>
        </div>
        <div class="cell colspan2">
            <div class="input-control text full-size">
                <input type="password" name="<?php echo $attr;?>">
            </div>
        </div>
    <!-- </div><br /> -->
<?php
}

// mise en place d'un filtre multiple a partir de 2 tables (la table en cours et la téble de reference)
function createInputSelectMultiple2Tables($mysql,$attribut,$table,$table2,$attribut2,$att_joint1,$att_joint2,$label,$liste=array())
{
    $sql_eq="select distinct `$table`.$attribut,`$table2`.$attribut2 as Libelle FROM `$table`,`$table2` where `$table`.$att_joint1=`$table2`.$att_joint2 order by `$table`.$attribut";
?>
<!--     <div class="row cells8">
 -->        
        <div class="cell">
            <div class="input-control"><?php echo $label;?></div>
        </div>
        <div class="cell colspan3">
            <div class="input-control select multiple full-size" style="height:100px;position: absolute;margin-top: -4px">
                <select multiple name="<?php echo $attribut;?>[]">
                    <option value="" disabled>Sélectionner un(e) <?php echo $label;?></option>
                    <?php
                    $req_eq=mysqli_query($mysql,$sql_eq);
                    while ($res=mysqli_fetch_object($req_eq))  
                    {
                        echo "<option value=".$res->$attribut;
                        if (in_array($res->$attribut, $liste)) echo " selected";
                        echo ">".$res->Libelle."</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
    <!-- </div><br /><br /> -->
 
<?php
}

function createInputSelect2Tables($mysql,$attribut,$table,$table2,$attribut2,$att_joint1,$att_joint2,$label,$liste=array())
{
    $sql_eq="select distinct `$table`.$attribut,`$table2`.$attribut2 as Libelle FROM `$table`,`$table2` where `$table`.$att_joint1=`$table2`.$att_joint2 order by `$table`.$attribut";
?>
    <!-- <div class="row cells8"> -->
        <div class="cell">
            <div class="input-control"><?php echo $label;?></div>
        </div>
        <div class="cell colspan3">
            <div class="input-control select full-size" style="position: absolute;margin-top: -4px">
                <select name="<?php echo $attribut;?>">
                    <option value="" disabled>Sélectionner un(e) <?php echo $label;?></option>
                    <?php
                    $req_eq=mysqli_query($mysql,$sql_eq);
                    while ($res=mysqli_fetch_object($req_eq))  
                    {
                        echo "<option value=".$res->$attribut;
                        if (in_array($res->$attribut, $liste)) echo " selected";
                        echo ">".$res->$attribut2."</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
    <!-- </div> -->
 
<?php
}

function createInputSelectMultiple($mysql,$id,$nomtable,$attr,$label,$liste=array()) { //Fonction permettant d'afficher une liste déroulante: il faut passer en paramètres la variable de connexion à la base de données, l'id, le nom et l'attribut à afficher de la table ainsi que le libelle à afficher à côté de l'input; $liste permet de récupérer la liste des valeurs qui doivent être pré-selectionnées
?>
    <!-- <div class="row cells8"> -->
        <div class="cell">
            <div class="input-control"><?php echo $label;?></div>
        </div>
        <div class="cell colspan3">
            <div class="input-control select multiple full-size" style="height:100px;position: absolute;margin-top: -4px">
                <select multiple name="<?php echo $attr;?>[]">
                    <option value="" disabled>Sélectionner un(e) <?php echo $label;?></option>
                    <?php
                    $req=mysqli_query($mysql,"SELECT DISTINCT $id,$attr FROM $nomtable ORDER BY $attr");
                    while ($res=mysqli_fetch_object($req))  
                    {
                        echo "<option value=".$res->$id;
                        if (in_array($res->$id, $liste)) echo " selected";
                        echo ">".$res->$attr."</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
    <!-- </div><br /><br /> -->
<?php
}

function createInputFile($attr,$label,$resultat="") {
    global $nom_table;
?>
    <!-- <div class="row cells8"> -->
        <div class="cell colspan1">
            <div class="input-control"><?php echo $label;?></div>
        </div>
        <div class="cell colspan2">
            <div class="input-control file" data-role="input">
                <input type="file" name="<?php echo $attr;?>">
                <button class="button"><span class="mif-folder"></span></button>
<?php            if ($resultat!='')  echo '<a href="./uploads/'.$nom_table.'/'.$resultat.'" target="blank">Fichier</a>';
?>            </div>
        </div>
        <div class="cell colspan2">
            <?php
            echo "<label class='input-control modern checkbox'>
                        <input type='checkbox' name='del$attr' id='del$attr'>
                        <span class='check'></span>
                        <span class='caption'>Supprimer le document</span>
                    </label>&nbsp;&nbsp;";
            ?>
        </div>

    <!-- </div><br /> -->
<?php
}


function createRadio($attr,$tab,$label,$resultat="") {
?>
    <!-- <div class="row cells7"> -->
        <div class="cell">
            <div class="input-control"><?php echo $label;?></div>
        </div>
        <div class="cell colspan2">
            <?php
            foreach ($tab as $val) {
                echo "<label class='input-control modern radio'>
                        <input type='radio' name='$attr' value='$val'";
                if ($val == $resultat)  echo " checked";
                echo ">
                        <span class='check'></span>
                        <span class='caption'>$val</span>
                    </label>&nbsp;&nbsp;";
            }
            ?>
        </div>
    <!-- </div> -->
<?php
}

function createCheckbox($tab,$label) {
?>
    <!-- <div class="row cells7"> -->
        <div class="cell">
            <div class="input-control"><?php echo $label;?></div>
        </div>
        <div class="cell colspan2">
            <?php
            foreach ($tab as $cle=>$val) {
                echo "<label class='input-control modern checkbox'>
                        <input type='checkbox' name='$cle'>
                        <span class='check'></span>
                        <span class='caption'>$val</span>
                    </label>&nbsp;&nbsp;";
            }
            ?>
        </div>
    <!-- </div><br /> -->
<?php
}

function setInfos($label,$val) {
    echo "<div class='list'>";
    if ($val=="")   $class="fg-red"; else $class="fg-green";
    echo "  <span class='list-title $class'>$label: $val</span>
        </div>";
}

function setGroupInfos($label,$tab) {
    echo "<div class='list-group collapsed'>
            <span class='list-group-toggle'>$label</span>
            <div class='list-group-content'>";
            foreach ($tab as $cle=>$value) {
                echo "<div class='list'>$cle : $value</div>";
            }
    echo "  </div>
        </div>";
}

function add_file($attr_name,$nom_table)
{
    // gestion du fichier
    $_FILES[$attr_name]['name'];     //Le nom original du fichier, comme sur le disque du visiteur (exemple : mon_icone.png).
    $_FILES[$attr_name]['type'];     //Le type du fichier. Par exemple, cela peut être « image/png ».
    $_FILES[$attr_name]['size'];     //La taille du fichier en octets.
    $_FILES[$attr_name]['tmp_name']; //L'adresse vers le fichier uploadé dans le répertoire temporaire.
    $_FILES[$attr_name]['error'];    //Le code d'erreur, qui permet de savoir si le fichier a bien été uploadé.
    if ($_FILES[$attr_name]['error'] > 0) $erreur = "Erreur lors du transfert";
    if ($_FILES[$attr_name]['error'] > 0) $erreur = "Erreur lors du transfert";
    $extension_upload = strtolower(  substr(  strrchr($_FILES[$attr_name]['name'], '.')  ,1)  );
    $name = $_FILES[$attr_name]['name'];
    $name=preg_replace("/ /","_",$name);
    $valide=0;
    if ($name!="")  $valide=1;
    // else print "pas de fichier";
    if ($valide==1)
        $resultat = move_uploaded_file($_FILES[$attr_name]['tmp_name'],"./uploads/$nom_table/$name");
}

function get($attr,$mult=false)
{
    $val="";
    if (isset($_GET[$attr]) && !empty($_GET[$attr]))    $val=preg_replace("/\'/", "\'", $_GET[$attr]);
    elseif (isset($_GET[$attr]))                        $val=preg_replace("/\'/", "\'", $_GET[$attr]);
    return                                              $val;
}

function post($attr,$mult=false)
{
    $val="";
    if (isset($_POST[$attr]) && !empty($_POST[$attr]))  $val=$_POST[$attr];
    elseif (isset($_POST[$attr]))                       $val=$_POST[$attr];
    return                                              $val;
}

function recherche($idBase,$table,$cle,$infoUSR,$info_rech) //permet de renvoyer l'info recherchée($info_rech) lorsque la cle dans la base ($cle) correspond à l'info donnée par l'user ($infoUSR)
{ 
    $req=mysqli_query($idBase,"SELECT * FROM $table WHERE $cle='".get($infoUSR)."'");
    $data = mysqli_fetch_array($req);
    return $data[$info_rech];
}

// permet de récupérer les options multiples d'un select lors d'un filtre
function get_multi_filtre($nomchamp,$Field="")
{
    global $conditions,$type;
    if ($Field=="") $Field=$nomchamp;
    $val="";
    if(isset($_GET[$Field]) && !empty($_GET[$Field])){
        $resp_array = $_GET[$Field];
        foreach($resp_array as $selectValue){
                $val.=",'".$selectValue."'";
        }
        $val=substr($val,1);
        if ($type=="p_urhaqdoc" && $Field=="Equipe")        $conditions.=" and Equipe in ($val) ";
        elseif ($Field=="Equipe")                           $conditions.=" and Eq in ($val) ";
        elseif ($val != "")                                $conditions.=" and $nomchamp in ($val)";
    }
}

// permet de récupérer l' option d'un select lors d'un filtre
function get_simple_filtre($nomchamp,$Field)
{
    global $conditions;
    if(isset($_GET[$Field]) && !empty($_GET[$Field])){
        $conditions.=" and $nomchamp in ('".$_GET[$Field]."')";
        }
}

// permet de récupérer la valeur d'un champ input lors d'un filtre
function get_text_filtre($nomchamp,$table="")
{
    global $conditions;
    if (isset($_GET[$nomchamp]))    
    {
        ${$nomchamp}=$_GET[$nomchamp];      
        if (${$nomchamp}!="")   
        {
            if ($table=="") $conditions .= " and $nomchamp like '%".${$nomchamp}."%'";
            else            $conditions .= " and $table.$nomchamp like '%".${$nomchamp}."%'";
        }
    }
}

// permet de récupérer la valeur d'un champ input lors d'un filtre
function get_date_filtre($nomchamp,$operande="=")
{
    global $conditions;
    if (isset($_GET[$nomchamp]))    
    {
        ${$nomchamp}=$_GET[$nomchamp];      
        if (${$nomchamp}!="")   
        {
            $conditions .= " and $nomchamp $operande '".${$nomchamp}."'";
        }
    }
}

function set_format($date)
{
    if (preg_match("/-/",$date))
    {
        list($a,$m,$j)=explode("-",$date);
        $date="$j/$m/$a";
    }
   
    return $date;
}
?>
