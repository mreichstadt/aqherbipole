<?php 
session_start();
include ("configuration.php");
include ("fonctions.php");
//Connexion � la base de donn�es
mysqli_connect(HOST,USER,MDP,DBNAME);

$sqltot=$_GET["sql"];
$sqltot=preg_replace("/\\\/","",$sqltot);
$sqltot=preg_replace("/like '!/","like '%",$sqltot);
$type=$_GET["type"];
if(preg_match("/ limit /",$sqltot))	list($sql,$limit)=explode("limit",$sqltot);
else 								$sql=$sqltot;
$tab=explode("where",$sql);
$select=$tab[0];
$where=$tab[1];

list($list_attr,$nom_table)=explode('from',$select);
$nom_table=trim($nom_table);
$tailleTab=count($tab); 
if ($tailleTab>2)
{
	for ($i=2;$i<$tailleTab;$i++)
		$where .= " where ".$tab[$i];
}
// list($select,$where)=explode("where",$sql);
// print "<br>$sql<br>";
$where=preg_replace("/d'/","d\\'",$where);
$where=preg_replace("/l'/","l\\'",$where);
 
$sql="$select where $where";

// print "<br>$sql<br>";
$req = mysqli_query($idBase,$sql); 
$attr=mysqli_fetch_fields($req);
$time=time();
header("Content-type: application/octet-stream");
// le fichier d'extraction est un fichier de type excel
// on va afficher les informations dans le tableur excel comme si il s'agissait d'un tableau simple en html
header("Content-Disposition: attachment; filename=extract$time.xls");
header("Pragma: no-cache");
header("Expires: 0");

// if (isset($_GET["explo"]))
// {
// 	if ($_GET["explo"]==6)	echo "<h1>Protocoles/Equipe</h1>";
// 	if ($_GET["explo"]==3)	echo "<h1>Protocoles UERT</h1>";
// }
echo "<table cellspacing='4' cellpadding='4' border=1 align=center><tr>";
if (mysqli_num_rows($req) != 0) 
{ 
	// titre des colonnes 
	$i = 0; 
	foreach($attr as $val)
	{
		$nomchamp=$val->name;
		if ($nom_table=="t_documentinterne_doi" && $nomchamp=="doi_commentaire")  		echo '<th>AuteursV1</th><th>AuteursNewV</th><th>Date</th><th>Document</th>';
		if (preg_match("/pro_die/", $nomchamp) || preg_match("/pro_prp/", $nomchamp))	echo '<th>'.strtoupper(substr($nomchamp,4)).'</th>';
		else 																			echo '<th>'.substr($nomchamp,4).'</th>';
	}
	echo "</tr>"; 

	// On lit les entr�es une � une gr�ce � une boucle
	while($donnees_messages=mysqli_fetch_assoc($req)) 
	{
		if (preg_match("/habilitations/",$sqltot))
		{
			$datejour = date("Ymd");
			if ($donnees_messages["Date_Fin"] !='')
			{
				list($a,$m,$j)=explode('-',$donnees_messages["Date_Fin"]);
				$datechamp=$a.$m.$j;
				if ($datejour>$datechamp && $donnees_messages["Date_Fin"]!='0000-00-00')
				{
					if ($donnees_messages["Date_Fin"]<$datejour && $donnees_messages["Date_depart_unite"]=='0000-00-00' && !preg_match("/autorisation � exp�rimenter/",$donnees_messages["Habilitation"]) 
					&& !preg_match("/Autorisation d\'exp�rimenter/",$donnees_messages["Habilitation"]) 
					&& !preg_match("/HDR/",$donnees_messages["Habilitation"]) && !preg_match("/niveau II/",$donnees_messages["Habilitation"]) 
					&& !preg_match("/niveau IV/",$donnees_messages["Habilitation"]))
						$style='style="background-color: #EBA68D;border: #DDEEFF 1px solid;"';
					else $style="";
				}
				else
					$style="";
			}
			else $style="";
		}
		echo "<tr>";
		foreach($attr as $val)
		{
			$nomchamp=$val->name;
			$infomess=$donnees_messages[$nomchamp];
			if($nomchamp=="doi_commentaire")
			{
				$req_version=mysqli_query($idBase,"select * from td_versiondocinterne_vdi where doi_id=".$donnees_messages["doi_id"]." order by vdi_nversion desc limit 0,1");
				if (mysqli_num_rows($req_version)==0) echo "<td></td><td></td><td></td><td></td>";
				else
				{
					$req_version2=mysqli_query($idBase,"select * from td_versiondocinterne_vdi where doi_id=".$donnees_messages["doi_id"]." order by vdi_nversion limit 0,1");
					$res_version2=mysqli_fetch_object($req_version2);
					$res_version=mysqli_fetch_object($req_version);
					echo "<td>".utf8_decode($res_version2->vdi_auteurs)."</td><td>".utf8_decode($res_version->vdi_auteurs)."</td><td>".set_format($res_version->vdi_date)."</td>";
					if (!isset($_SESSION["UserLevel"])) 			echo "<td>Fichier</td>";
					elseif ($res_version->vdi_document != "")		echo '<td><a href="https://www1.clermont.inra.fr/appurh/AQHerbipole/uploads/td_versiondocinterne_vdi/'.$res_version->vdi_document.'" target="blank">Fichier</a></td>';
					else											echo '<td>'.$res_version->vdi_document.'</td>';
					mysqli_free_result($req_version2);
				}
				mysqli_free_result($req_version);
				echo '<td>'.utf8_decode($donnees_messages[$nomchamp]).'</td>';				
			}
			elseif ($nomchamp != "Photo") 
			{
				echo '<td>'.utf8_decode($donnees_messages[$nomchamp]).'</td>';
			}
		}
		// if (preg_match("/habilitations/",$type) && $nomchamp=="Date_depart_unite")
		// {
		// 	$req_fp=mysqli_query($idBase,"select count(*) nb from a_fp where IDhab=".$donnees_messages["Id"]);
		// 	$res_fp=mysqli_fetch_object($req_fp);
		// 	$nb_fp=$res_fp->nb;
		// }	echo "<td>$nb_fp</td>";
		echo "</tr>";
	}
} 
?> 
</table>