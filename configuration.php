<?php
define("HOST","localhost");
define("USER","root");
define("MDP","");
define("DBNAME","aqherbipole");
// define("USER","aqurh");
// define("MDP","7NR-tmgt");
// define("DBNAME","aqurh");
define("TITLE_HEADER","Assur@nce Qualit&eacute; UE 1414 Herbipôle");
define("SUB_HEADER","");
define("WEBMASTER","M. Reichstadt");
define("MAIL_WEBMASTER","matthieu.reichstadt@inra.fr");
// noms des differents onglets
// si  on veut supprimer un onglet il faut commenter la ligne de definition de la constante
define("NOM_ONGLET_MANAGEMENT","Management");
define("NOM_ONGLET_DOCUMENTATION","Documentation");
define("NOM_ONGLET_RESSOURCES","Ressources Humaines");
define("NOM_ONGLET_EQUIPEMENTS","Mat&eacute;riels");
define("NOM_ONGLET_EXPLORATION","Exploration");
define("NOM_ONGLET_REALISATIONS","R&eacute;alisations");
define("NOM_ONGLET_VUES","Vues base");
define("NOM_ONGLET_ADMINISTRATION","Administration");
define("NOM_ONGLET_CONNEXION","Connexion");
define("NOM_ONGLET_REGLEMENTATION","R&eacute;glementation");
$idBase=mysqli_connect(HOST,USER,MDP,DBNAME);
mysqli_set_charset($idBase, "utf8");
mysqli_query($idBase,"SET NAMES utf8" );

$tab_document=array("ele_document","eea_document","agr_document","crs_document","dre_document","gcs_lettremission","tcs_document","plc_document","ccs_document","dcs_document","grp_lettremission","sup_document","dop_document","coi_document","coe_document","req_document","paq_document","oaq_document","gaq_lettremission","aea_document","pla_document","aua_document","cra_document","doe_document","hab_document","fee_feuillepresence","faq_feuillepresence","sta_rapport","mad_facture","mad_modeemploi","mah_rapport","pro_die","pro_dossierdemande","pro_depotapafis","pro_avisethique","pro_autorisationministere","pro_protocole","pro_livraisonrendu","pro_retourexpe","vep_document","vdi_document","vdi_docvalidation");
$tab_photo=array("grs_photo","gcs_photo","grp_photo","mar_photo","mas_photo","gaq_photo","sta_photo");
$tab_radio=array("mar_conforme","mas_conforme");
$tab_date=array("ele_applicabledepuis","eea_applicabledepuis","agr_delivrele","agr_valable","grs_debut","grs_fin","crs_ajoutele","dre_date","gcs_debut","gcs_fin","tcs_date","plc_date","dcs_date","ccs_ajoutele","grp_debut","grp_fin","sup_date","dop_date","coi_ajoutele","coe_date","cla_debut","cla_fin","ech_debut","ech_fin","mar_datereforme","req_date","paq_apartirde","paq_jusque","oaq_apartir","oaq_jusque","gaq_debut","gaq_fin","aea_date","pla_date","aua_date","cra_ajoutele","doi_obsolete","doe_dateparution","hab_delivrele","hab_jusque","hab_datedepart","fee_date","faq_date","pro_debut","pro_fin","mad_enservicele","mad_fingarantie","mah_faitle","vep_date","vdi_date");
$tab_site=array("agr_site","grs_site","crs_site","gcs_site","tcs_site","plc_site","grp_site","sup_site","mar_site","mar_equipe","mas_site","pro_site","gaq_site","aea_site","pla_site","aua_site","doi_site","doi_equipe","fee_site","sta_site","cla_site","cla_equipe","pro_equipe","hab_equipe","faq_equipe","hab_habilitation","doi_forme","ech_rubrique","mar_nature","mar_marque","mas_nature","mas_marque","cla_modele","hab_habilitation","doe_origine","doe_type");
$tabRef=array(
	"site"=>array("table"=>"tr_site_sit","id"=>"sit_id","libelle"=>"sit_libelle"),
	"equipe"=>array("table"=>"tr_equipe_equ","id"=>"equ_id","libelle"=>"equ_libelle"),
	"habilitation"=>array("table"=>"tr_habilitations_rha","id"=>"rha_id","libelle"=>"rha_libelle"),
	"forme"=>array("table"=>"tr_forme_for","id"=>"for_id","libelle"=>"for_libelle"),
	"rubrique"=>array("table"=>"tr_rubrique_rub","id"=>"rub_id","libelle"=>"rub_libelle"),
	"nature"=>array("table"=>"tr_nature_nat","id"=>"nat_id","libelle"=>"nat_libelle"),
	"marque"=>array("table"=>"tr_marque_mar","id"=>"mar_id","libelle"=>"mar_libelle"),
	"modele"=>array("table"=>"tr_modele_mod","id"=>"mod_id","libelle"=>"mod_libelle"),
	"habilitation"=>array("table"=>"tr_habilitations_rha","id"=>"rha_id","libelle"=>"rha_libelle"),
	"origine"=>array("table"=>"tr_originetxt_ort","id"=>"ort_id","libelle"=>"ort_libelle"),
	"type"=>array("table"=>"tr_typetxt_tyt","id"=>"tyt_id","libelle"=>"tyt_libelle"),
	"application"=>array("table"=>"tr_application_app","id"=>"app_id","libelle"=>"app_libelle"),
);

function stripAccents($string){
	return strtr($string,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ',
'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
}
?>