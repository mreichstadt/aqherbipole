<?php 
$error = "";

if(isset($_POST['try_conn']))
{
    $username = $_POST['user_login']; 
    $password = $_POST['user_password'];     
    $sqlLogin="select * from `admin_droits` where user='$username' and pwd='$password'";
    $reqLogin=mysqli_query($idBase,$sqlLogin) or die("pb reqs".mysqli_error($idBase));
    if(mysqli_num_rows($reqLogin)>0)
    {
        $_SESSION['user'] = $username;
        $_SESSION['login'] = true; 
    }
    // si on veut utiliser le ldap
    else
    {     
        $ldap_host = "ldap-authentification.inra.fr";  //inserer ici l'addresse du serveur LDAP
        $base_dn = "ou=personnes,dc=inra,dc=fr";

        $user="uid=$username";
        
        $connect = ldap_connect($ldap_host) or exit(">>Connexion au serveur LDAP echou�<<");
                 
        ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);  // on passe le LDAP en version 3, necessaire pour travailler avec le AD
        ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);

        $read = ldap_search($connect,$base_dn, $user) or exit(">>erreur lors de la recherche<<");
        $info = ldap_get_entries($connect, $read);
        if (count($info)>1)     $bind = ldap_bind($connect,$info[0]["dn"],$password);
        else                    $bind=FALSE;
        //$bind=true;
        if ( $bind == FALSE )       // si le BIND est FALSE, le mot de passe est erron�e
        {
            $error=1;
        }
        else    // on peut ajouter d'autre traitement si l'identification est ok ( ex : $_SESSION['user'] = ... )
        {
            $_SESSION['user'] = $username;
            $_SESSION['login'] = true; 
        }
    }
    if ($error != 1) {
    	$sql="select user,UserLevel from `admin_droits` where user='".$_POST["user_login"]."'";
		$req=mysqli_query($idBase,$sql);
		$nb=mysqli_num_rows($req);
		if ($nb>0)
		{
			$res=mysqli_fetch_object($req);
			$UserLevel=$res->UserLevel;
			$_SESSION["UserLevel"]=$UserLevel;
		}
        else $_SESSION["UserLevel"]=0;
		$date=time();
		$req=mysqli_query($idBase,"insert into admin_utilisation(user,date) values('$user','$date')");	
        ?>
        <meta http-equiv="refresh" content="0;URL=index.php">
        <?php
    }
}
 
?>
<style>
        .login-form {
            width: 25rem;
            height: 18.75rem;
            position: fixed;
            top: 50%;
            margin-top: -9.375rem;
            left: 50%;
            margin-left: -12.5rem;
            background-color: #ffffff;
            opacity: 0;
            -webkit-transform: scale(.8);
            transform: scale(.8);
        }
</style>

<script>
$(function(){
            var form = $(".login-form");

            form.css({
                opacity: 1,
                "-webkit-transform": "scale(1)",
                "transform": "scale(1)",
                "-webkit-transition": ".5s",
                "transition": ".5s"
            });
        });
</script>


<?php
if (!isset($_SESSION["user"])) {
?>
<body class="bg-darkTeal">
    <?php if ($error) {
    ?>
    <div class="grid">
        <div class="row cells3">                
            <div class="cell"></div>
            <div class="cell"><center>
                <div class="notify alert">
                    <span class="notify-title">Erreur</span>
                    <span class="notify-text">Login ou mot de passe incorrect</span>
                </div></center>
            </div>
            <div class="cell"></div>
        </div>
    </div>
    <?php } ?>
<div class="login-form padding20 block-shadow">
    <form name = "input" method = "post" action="index.php">
        <h1 class="text-light">Login</h1>
        <hr class="thin"/>
        <br />
        <div class="input-control text full-size" data-role="input">
            <label for="user_login">User name:</label>
            <input type="text" name="user_login" id="user_login">
            <button class="button helper-button clear"><span class="mif-cross"></span></button>
        </div>
        <br />
        <br />
        <div class="input-control password full-size" data-role="input">
            <label for="user_password">User password:</label>
            <input type="password" name="user_password" id="user_password">
            <button class="button helper-button reveal"><span class="mif-looks"></span></button>
        </div>
        <br />
        <br />
        <div class="form-actions">
            <button type="submit" class="button primary" value = "Login" name = "try_conn">Login</button>
            <a href="index.php"><button type="button" class="button link">Cancel</button></a>
        </div>
    </form>
</div>

<?php
}
if(isset($_GET["connexion"])) {
?>
<body>
<?php

unset($_SESSION["user"]);
unset($_SESSION["login"]);
?>
        <meta http-equiv="refresh" content="0;URL=index.php">
        <?php
}// exit();
?>
</body>

</html>