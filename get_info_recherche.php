<?php
	$fieldset_width="450px";
?>
<h3><center>Recherche d'informations</h3></center>
<form action="./" method="get">
<input type="hidden" name="info" value="<?php echo $info;?>">
<div class="example" data-text="Recherche">
	<div class="grid">
		<div class="row cells8">
<?php
	createInputText("iddoc","IdDoc");
	createInputText("titre","Titre");
?>
		</div><div class="row cells8">
<?php
	createInputText("auteur","Nom Personne");
	// createInputText("num_materiel","N&deg; Mat&eacute;riel");
?>
		</div>
	<input type="submit" value="Rechercher"><br />
	</div>
</div>
</form>
<hr>
<?php
if ((isset($_GET["iddoc"]))||(isset($_GET["titre"]))||(isset($_GET["auteur"]))||(isset($_GET["num_materiel"])))
{
	if (isset($_GET["iddoc"]))			$iddoc=$_GET["iddoc"];
	if (isset($_GET["titre"]))			$titre=$_GET["titre"];
	if (isset($_GET["auteur"]))			$auteur=$_GET["auteur"];
	// if (isset($_GET["num_materiel"]))	$num_materiel=$_GET["num_materiel"];
		
	// nombre de docs internes
	if ($iddoc!="") $cond_id=" t_documentinterne_doi.doi_id = $iddoc ";
	else 			$cond_id="1";
	// $cond_docs_internes="1 and p1.vdi_id>=ALL(select vdi_id from td_versiondocinterne_vdi p2 where p1.doi_id=p2.doi_id and p2.vdi_auteurs like '%$auteur%') and p1.doi_id=t_documentinterne_doi.doi_id and doi_titre like '%$titre%' and $cond_id ";
	$cond_docs_internes="1 and exists(select vdi_auteurs from td_versiondocinterne_vdi where vdi_auteurs like '%$auteur%' and td_versiondocinterne_vdi.doi_id=t_documentinterne_doi.doi_id) and doi_titre like '%$titre%' and $cond_id ";
	$sql="select count(*) nb from t_documentinterne_doi where $cond_docs_internes";
	if ($_SESSION["user"]=="uid=mreichstadt") echo $sql;
	$req=mysqli_query($idBase,$sql);	$res=mysqli_fetch_object($req);	$nb_docs_internes=$res->nb;
	if ($nb_docs_internes>0)	$lien_docs_internes='<a href="index.php?info=t_documentinterne_doi&conditions='.$cond_docs_internes.'">Interne('.$nb_docs_internes.')</a>';
	else						$lien_docs_internes='Interne('.$nb_docs_internes.')';
	
	// // nombre de docs externes
	if ($iddoc!="") $cond_id=" doe_id = $iddoc ";
	else 			$cond_id="1";
	$cond_docs_externes="1 and doe_titre like '%$titre%' and $cond_id";
	$sql="select count(*) nb from t_documentexterne_doe where $cond_docs_externes";
	// if ($_SESSION["UserLevel"]==-1) print $sql;
	$req=mysqli_query($idBase,$sql);	$res=mysqli_fetch_object($req);	$nb_docs_externes=$res->nb;
	if ($nb_docs_externes>0)	$lien_docs_externes='<a href="index.php?info=t_documentexterne_doe&conditions='.$cond_docs_externes.'">Externe('.$nb_docs_externes.')</a>';
	else						$lien_docs_externes='Externe('.$nb_docs_externes.')';
	
	// nombre de formations
	if ($iddoc!="") $cond_id=" faq_id = $iddoc ";
	else 			$cond_id="1";
	$cond_formations="1 and faq_titre like '%$titre%' and $cond_id and faq_organisateur like '%$auteur%'";
	$sql="select count(*) nb from t_formationaq_faq where $cond_formations";
	$req=mysqli_query($idBase,$sql);	$res=mysqli_fetch_object($req);	$nb_formations=$res->nb;
	if ($nb_formations>0)	$lien_formations='<a href="index.php?info=t_formationaq_faq&conditions='.$cond_formations.'">Formations('.$nb_formations.')</a>';
	else					$lien_formations='Formations('.$nb_formations.')';

	// // nombre de réalisations
	if ($iddoc!="") $cond_id=" pro_id = $iddoc ";
	else 			$cond_id="1";
	$cond_real="1 and pro_titre like '%$titre%' and $cond_id and pro_commanditaire like '%$auteur%'";
	$sql="select count(*) nb from t_protocoles_pro where $cond_real";
	$req=mysqli_query($idBase,$sql);	$res=mysqli_fetch_object($req);	$nb_real=$res->nb;
	if ($nb_real>0)		$lien_real='<a href="index.php?info=t_protocoles_pro&conditions='.$cond_real.'">Protocoles('.$nb_real.')</a>';
	else				$lien_real='Protocoles('.$nb_real.')';
	$lien_docs_internes=preg_replace("/like '%/","like '!",$lien_docs_internes);$lien_docs_internes=preg_replace("/%'/","!'",$lien_docs_internes);
	$lien_docs_externes=preg_replace("/like '%/","like '!",$lien_docs_externes);$lien_docs_externes=preg_replace("/%'/","!'",$lien_docs_externes);
	$lien_formations=preg_replace("/like '%/","like '!",$lien_formations);$lien_formations=preg_replace("/%'/","!'",$lien_formations);
	$lien_real=preg_replace("/like '%/","like '!",$lien_real);$lien_real=preg_replace("/%'/","!'",$lien_real);

	// nombre de matériels
	if ($iddoc!="") $cond_id=" mar_id = $iddoc ";
	else 			$cond_id="1";
	$sql="select count(*) nb from t_materiel_mat where $cond_id";
	$req=mysqli_query($idBase,$sql);	$res=mysqli_fetch_object($req);	$nb_doc_mat=$res->nb;
	if ($nb_doc_mat>0)	$lien_mat='<a href="index.php?info=t_materiel_mat&conditions='.$cond_id.'">Mat&eacute;riel ('.$nb_doc_mat.')</a>';
	else				$lien_mat='Mat&eacute;riel ('.$nb_doc_mat.')';
	$lien_mat=preg_replace("/like '%/","like '!",$lien_mat);$lien_mat=preg_replace("/%'/","!'",$lien_mat);
	
?>
<center><h3>R&eacute;sultats de la Recherche:</h3></center>
<ul>
	<li><font class="ul_recherche">Mat&eacute;riel</font>
		<ul>
			<li><?php echo $lien_mat;?></li>
		</ul>
	</li><br>
	<li><font class="ul_recherche">Documentation</font>
		<ul>
			<li><?php echo $lien_docs_internes;?></li>
			<li><?php echo $lien_docs_externes;?></li>
		</ul>
	</li><br>
	<li><font class="ul_recherche">Ressources Humaines</font>
		<ul>
			<li><?php echo $lien_formations;?></li>
		</ul>
	</li><br>
	<li><font class="ul_recherche">R&eacute;alisations</font>
		<ul>
			<li><?php echo $lien_real?></li>
		</ul>
	</li>	
</ul>
<?php
}
?>

