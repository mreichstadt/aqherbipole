<form action="./index.php" method="get">
<input type="hidden" name="info" value="<?php echo $infos;?>">
<input type="hidden" name="type" value="<?php echo $type;?>">
<div class="accordion"" data-role="accordion">
	<div class="frame">
		<div class="heading">Filtre</div>
		<div class="content">
			<div class="grid">
				<div class="row cells8">

<?php
	switch ($nom_table)
	{
	// REGLEMENTATION
		case "t_elevage_ele":
			createInputText("ele_titre","Titre");
			break;
		case "t_ethiqueexpeanimale_eea":
			createInputText("eea_titre","Titre");
			break;
		case "t_agrements_agr":
			createInputText("agr_titre","Titre");
			createInputSelectMultiple2Tables($idBase,"agr_site",$nom_table,"tr_site_sit","sit_libelle","agr_site","sit_id","Site");
			break;

	// SBEA
		case "t_groupesbea_grs": 
			createInputText("grs_nomprenom","Nom/Prénom");
			createInputSelectMultiple2Tables($idBase,"grs_site",$nom_table,"tr_site_sit","sit_libelle","grs_site","sit_id","Site");
			break;
		case "t_crreunionssbea_crs":  
			createInputText("crs_intitule","Intitule");
			createInputSelectMultiple2Tables($idBase,"crs_site",$nom_table,"tr_site_sit","sit_libelle","crs_site","sit_id","Site");
			break;
		case "t_docrefreth_dre": 
			createInputText("dre_titre","Titre");
			break;

	// CHARTE SANITAIRE
		case "t_groupecs_gcs": 
			createInputText("gcs_nomprenom","Nom/Prénom");
			createInputSelectMultiple2Tables($idBase,"gcs_site",$nom_table,"tr_site_sit","sit_libelle","gcs_site","sit_id","Site");
			break;
		case "t_suivics_tcs":  
			createInputText("tcs_intitule","Intitule");
			createInputSelectMultiple2Tables($idBase,"tcs_site",$nom_table,"tr_site_sit","sit_libelle","tcs_site","sit_id","Site");
			break;
		case "t_planactioncs_pls":   
			createInputText("pls_intitule","Intitule");
			createInputSelectMultiple2Tables($idBase,"pls_site",$nom_table,"tr_site_sit","sit_libelle","pls_site","sit_id","Site");
			break;
		case "t_crreunionscs_ccs": 
			createInputText("ccs_intitule","Intitule");
			break;
		case "t_documentationcs_dcs":  
			createInputText("dcs_titre","Titre");
			break;

	// DOCUMENTATION AQ
		case "t_documentinterne_doi":
			createInputSelectMultiple2Tables($idBase,"doi_equipe",$nom_table,"tr_equipe_equ","equ_libelle","doi_equipe","equ_id","Equipe");
			createInputSelectMultiple2Tables($idBase,"doi_forme",$nom_table,"tr_forme_for","for_libelle","doi_forme","for_id","Forme");
			?></div><div class="row cells8"><?php
			createInputText("doi_titre","Titre");
			break;
		case "t_documentexterne_doe":
			createInputSelectMultiple2Tables($idBase,"doe_application",$nom_table,"tr_application_app","app_libelle","doe_application","app_id","Application");
			createInputSelectMultiple2Tables($idBase,"doe_origine",$nom_table,"tr_originetxt_ort","ort_libelle","doe_origine","ort_id","OrigineTxt");
			?></div><div class="row cells8"><?php
			createInputSelectMultiple2Tables($idBase,"doe_type",$nom_table,"tr_typetxt_tyt","tyt_libelle","doe_type","tyt_id","TypeTxt");
			createInputText("doe_titre","doe_titre");
			break;

	// RESSOURCES HUMAINES
		case "t_habilitations_hab":  
			createInputSelectMultiple2Tables($idBase,"hab_equipe",$nom_table,"tr_equipe_equ","equ_libelle","hab_equipe","equ_id","Equipe");
			createInputSelectMultiple2Tables($idBase,"hab_habilitation",$nom_table,"tr_habilitations_rha","rha_libelle","hab_habilitation","rha_id","Habilitations");
			?></div><div class="row cells8"><?php
			createInputText("hab_nomprenom","Nom/Prénom");
			break;
		case "t_feea_fee":   
			createInputText("fee_nomprenom","Nom/Prénom");
			createInputSelectMultiple2Tables($idBase,"fee_site",$nom_table,"tr_site_sit","sit_libelle","fee_site","sit_id","Site");
			?></div><div class="row cells8"><?php
			createInputText("fee_titre","Titre");
			break;
		case "t_formationaq_faq":    
			createInputText("faq_nomprenom","Nom/Prénom");
			createInputSelectMultiple2Tables($idBase,"faq_equipe",$nom_table,"tr_equipe_equ","equ_libelle","faq_equipe","equ_id","Equipe");
			?></div><div class="row cells8"><?php
			createInputText("fee_titre","Titre");
			break;
		case "t_stagiaires_sta":    
			createInputText("sta_nomprenom","Nom/Prénom");
			createInputSelectMultiple2Tables($idBase,"sta_site",$nom_table,"tr_site_sit","sit_libelle","sta_site","sit_id","Site");
			break;

	// PREVENTION
		case "t_groupeprevention_grp": 
			createInputText("grp_nomprenom","Nom/Prénom");
			createInputSelectMultiple2Tables($idBase,"grp_site",$nom_table,"tr_site_sit","sit_libelle","grp_site","sit_id","Site");
			break;
		case "t_suiviprevention_sup":  
			createInputSelectMultiple2Tables($idBase,"sup_site",$nom_table,"tr_site_sit","sit_libelle","sup_site","sit_id","Site");
			break;
		case "t_documentationprevention_dop":  
			createInputText("dop_titre","Titre");
			break;

	// MANAGEMENT QUALITE
		case "t_referentielqualite_req":   
			createInputText("req_titre","Titre");
			break;
		case "t_politiqueaq_paq":    
			createInputText("paq_titre","Titre");
			break;
		case "t_organisationaq_oaq":     
			createInputText("oaq_titre","Titre");
			break;
		case "t_groupeaq_gaq":     
			createInputText("gaq_nomprenom","Nom/Prénom");
			createInputSelectMultiple2Tables($idBase,"gaq_site",$nom_table,"tr_site_sit","sit_libelle","gaq_qite","sit_id","Site");
			break;
		case "t_autoevalaq_aea":      
			createInputText("aea_intitule","Intitule");
			createInputSelectMultiple2Tables($idBase,"aea_site",$nom_table,"tr_site_sit","sit_libelle","aea_site","sit_id","Site");
			break;
		case "t_planactionaq_pla":       
			createInputText("pla_intitule","Intitule");
			createInputSelectMultiple2Tables($idBase,"pla_site",$nom_table,"tr_site_sit","sit_libelle","pla_site","sit_id","Site");
			break;
		case "t_auditsaq_aua":        
			createInputText("aua_intitule","Intitule");
			createInputSelectMultiple2Tables($idBase,"aua_site",$nom_table,"tr_site_sit","sit_libelle","aua_site","sit_id","Site");
			break;
		case "t_crreunionsaq_cra":         
			createInputText("cra_intitule","Intitule");
			break;

	// MATERIEL
		case "t_materiel_mat":
			createInputSelectMultiple2Tables($idBase,"mar_site",$nom_table,"tr_site_sit","sit_libelle","mar_site","sit_id","Site");
			createInputSelectMultiple2Tables($idBase,"mar_equipe",$nom_table,"tr_equipe_equ","equ_libelle","mar_equipe","equ_id","Equipe");
			?></div><div class="row cells8"><?php
			createInputSelectMultiple2Tables($idBase,"mar_nature",$nom_table,"tr_nature_nat","nat_libelle","mar_nature","nat_id","Nature");
			createInputSelectMultiple2Tables($idBase,"mar_marque",$nom_table,"tr_marque_mar","mar_libelle","mar_marque","mar_id","Marque");
			break;

	// EXPERIMENTATIONS
		case "t_protocoles_pro":      
			createInputText("pro_titre","Titre");
			createInputSelectMultiple2Tables($idBase,"pro_site",$nom_table,"tr_site_sit","sit_libelle","pro_site","sit_id","Site");
			?></div><div class="row cells8"><?php
			createInputSelectMultiple2Tables($idBase,"pro_equipe",$nom_table,"tr_equipe_equ","equ_libelle","pro_equipe","equ_id","Equipe");
			break;
		case "t_cahierlabo_cla":       
			createInputText("cla_intitule","Intitule");
			createInputSelectMultiple2Tables($idBase,"cla_site",$nom_table,"tr_site_sit","sit_libelle","cla_site","sit_id","Site");
			?></div><div class="row cells8"><?php
			createInputSelectMultiple2Tables($idBase,"cla_equipe",$nom_table,"tr_equipe_equ","equ_libelle","cla_equipe","equ_id","Equipe");
			break;
		case "t_echantillons_ech":         
			createInputSelectMultiple2Tables($idBase,"ech_site",$nom_table,"tr_site_sit","sit_libelle","ech_site","sit_id","Site");
			break;

	// COMMUNICATION
		case "t_communicationinterne_coi":        
			createInputText("coi_titre","Titre");
			break;
		case "t_communicationexterne_coe":        
			createInputText("coe_titre","Titre");
			break;
	}
?>		
				</div>
				<div class="row cells8">
					<?php createInputText("$id_attr","Id");?>
				</div>
				<br />	   					
				<input type="submit" value="rechercher"></form> <a href="./?infos=<?php echo $infos;?>&type=<?php echo $type;?>"> <button class="button primary">RAZ</button></a>
			</div>
		</div>
	</div>
</div>
</form>