<?php
$list_attr="select 	YEAR(Deb) as Annee, COUNT(*) as Total, COUNT( DISTINCT rapport_ethique ) AS 'Nb Rapports Ethiques', 
					COUNT( DISTINCT if(Declaration_intention_devis!='',Declaration_intention_devis,null) ) AS 'Nb Declaration_intention_devis', 
					COUNT( DISTINCT  if(`Retour_experience`!='',`Retour_experience`,null) ) AS 'Nb Retour_experience' 
			from s_protocol";
$conditions=" where 1";
$conditionsbase=$conditions;
$group=" GROUP BY YEAR(Deb)  ORDER BY year(Deb)";

$filtre=0;
if (isset($_GET["conditions"])) $conditions=$_GET["conditions"];

if(isset($_GET['Annee'])) 
{
	if ($_GET['Annee']!="All")	{$conditions.=" and year(Deb) ='".$_GET['Annee']."'";$filtre=1;}
}
if(isset($_GET['code_annee']) && !empty($_GET['code_annee']))
{
	$equipe="";
	$eq_array = $_GET['code_annee'];
	foreach($eq_array as $selectValue){
		$equipe.=",'".$selectValue."'";
	}
	$equipe=substr($equipe,1);
	$equipe="(".$equipe.")";
	$conditions.=" and year(Deb) in $equipe ";
}
// requete
$sql_mess="$list_attr $conditions $group";
$retour_messages=mysqli_query($idBase,$sql_mess);
$attr=mysqli_fetch_fields($retour_messages);
$total=mysqli_num_rows($retour_messages);
if ($_SESSION["user"]=="uid=mreichstadt") echo $sql_mess;
?>
<form action="./index.php" method="get" name="recherche_gauche">
<input type="hidden" name="exploration" value="2">
<div class="example" data-text="Filtre">
	<div class="grid">
	    <div class="row cells8">
	        <div class="cell">
	            <div class="input-control">Ann&eacute;e</div>
	        </div>
	        <div class="cell colspan3">
	            <div class="input-control select full-size" style="position: absolute;margin-top: -4px">
				<select name="code_annee[]" multiple STYLE="height:100px;width:200px">
				<?php
				$req_cpc=mysqli_query($idBase,'SELECT distinct year(Deb) as Annee FROM `s_protocol` order by year(Deb) desc');
				while ($res_cpc=mysqli_fetch_object($req_cpc))
				{
					$code_equipe=$res_cpc->Annee;
					print "<option value='$code_equipe'>$code_equipe</option>";
				}
				?>
				</select>
				</div>
			</div>
		</div>
<br /><input type="submit" value="Filtrer"><br />
	</div>
</div>
</form><br>
<?php
$sql_mess2=preg_replace("/like '%/","like '!",$sql_mess);
?>	
<a href="export_csv.php?sql=<?php echo $sql_mess2;?>"><button class="button success"><span class="mif-download"></span> Export excel</button></a>
<?php
$nb=0;
if ($total>0)
{
	echo '<table class="table striped hovered cell-hovered border"  id="maTable"><thead><tr>';
	foreach ($attr as $val)	echo "<th>".$val->name."</th>";
	echo "<th>Nb Protocoles</th></tr></thead><tbody>";
	while($donnees_messages=mysqli_fetch_row($retour_messages)) // On lit les entr�es une � une gr�ce � une boucle
	{
		echo "<tr><td>".$donnees_messages[0]."</td><td>".$donnees_messages[1]."</td><td>".($donnees_messages[2] )."</td><td>".($donnees_messages[3] )."</td><td>".($donnees_messages[4])."</td>";
		echo "<td>";
		$nbP=0;
		$sqlProt="select count(distinct IDDoc) as tot from p_version_proto where IDDoc in (select Id from s_protocol where YEAR(Deb)='".$donnees_messages[0]."')";
		$reqProt=mysqli_query($idBase,$sqlProt);
		$resProt=mysqli_fetch_object($reqProt);
		echo $resProt->tot."</td></tr>";
	}
	echo "</tbody></table>";
}
?>
<br /><br />
<?php
if ($filtre==1)
{
	include("./FusionCharts/FusionCharts.php");
	echo "<center><table border=0><tr>";
	$nb=0;
	$strXML = "<chart labelDisplay='Rotate' slantLabels='1' formatNumber='0' formatNumberScale='0' caption='Situation pour ".$_GET["Annee"]."' xAxisName='Nombre' canvasBgAlpha='30' yAxisName='Total' showValues='1' 
	alternateHGridColor='FCB541' alternateHGridAlpha='20' divLineColor='FCB541' divLineAlpha='50' canvasBorderColor='666666' baseFontColor='666666' lineColor='FCB541'>";

	$cle=$_GET["Annee"];
	$sql= "$list_attr where YEAR(Deb)='$cle' $group";
	// echo $sql;
	$req = mysqli_query($idBase,$sql);
	$retour_messages=mysqli_query($idBase,$sql);
	$total=0;
	$res=mysqli_fetch_object($retour_messages);
	while ($field = mysql_fetch_field($req))	
	{
		$nomchamp=$field->name;
		$value=$res->$nomchamp;
		if ($nomchamp!='Total')	$value--;
		if ($field->name!="Annee")
		{
			switch ($field->name)
			{
				case "Nb Rapports Ethiques":
					$color="0000FF";
					break;
				case "Nb Declaration_intention_devis":
					$color="003333";
					break;
				case "Total":
					$color="FF0000";
					break;
				default:
					$color="00FF00";
					break;
			}
			$strXML .= "<set color='".$color."' label='".$field->name."' value='".$value."' />";
		}
	}
	$strXML .= "</chart>";
	if ($nb%2==0)	echo "</tr><tr>";
	echo "<td>".renderChartHTML("./FusionCharts/Column3D.swf", "", $strXML, "myNext", 600, 400, false);
	echo "</td>";
	$nb++;
	echo "</tr></table></center>";
}