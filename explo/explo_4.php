<?php
$list_attr="SELECT year(Date) as Annee,Equipe,Document from p_urhaqdoc left join p_version ON p_urhaqdoc.Id = p_version.IDdoc";
$conditions=" where Forme like 'Mode op%' and Obsolete_le='Non'";
$list_attr="SELECT year(Date) as Annee,Equipe,Document from p_urhaqdoc,p_version p1";
$conditions=" where (p1.IDversion>=ALL(select IDversion from p_version p2 where p1.IDdoc=p2.IDdoc) or p1.Date is NULL) and p1.IDdoc=p_urhaqdoc.Id and Forme like 'Mode op%' and (Obsolete_le='Non' or Obsolete_le>date(now()))";

$conditionsbase=$conditions;
$group=" ORDER BY year(Date)";

$filtre=0;
if (isset($_GET["conditions"])) $conditions=$_GET["conditions"];

if(isset($_GET['Annee'])) 
{
	if ($_GET['Annee']!="All")	{$conditions.=" and year(Date) ='".$_GET['Annee']."'";$filtre=1;}
}
if(isset($_GET['Equipe'])) 
{
	if ($_GET['Equipe']!="All")	{$conditions.=" and Equipe ='".$_GET['Equipe']."'";$filtre=1;}
}
if(isset($_GET['code_annee']) && !empty($_GET['code_annee']))
{
	$equipe="";
	$eq_array = $_GET['code_annee'];
	foreach($eq_array as $selectValue){
		$equipe.=",'".$selectValue."'";
	}
	$equipe=substr($equipe,1);
	$equipe="(".$equipe.")";
	$conditions.=" and year(Date) in $equipe ";
}
if(isset($_GET['code_equipe']) && !empty($_GET['code_equipe']))
{
	$equipe="";
	$eq_array = $_GET['code_equipe'];
	foreach($eq_array as $selectValue){
		$equipe.=",'".$selectValue."'";
	}
	$equipe=substr($equipe,1);
	$equipe="(".$equipe.")";
	$conditions.=" and Equipe in $equipe ";
}

// requete
$sql_mess="$list_attr $conditions $group";
if (preg_match("/mreichstadt/",$_SESSION["user"])) echo $sql_mess;

$retour_messages=mysqli_query($idBase,$sql_mess);
$attr=mysqli_fetch_fields($retour_messages);
$total=mysqli_num_rows($retour_messages);

?>

<form action="./index.php" method="get" name="recherche_gauche">
<input type="hidden" name="exploration" value="4">
<div class="example" data-text="Filtre">
	<div class="grid">
	    <div class="row cells8">
	        <div class="cell">
	            <div class="input-control">Equipe</div>
	        </div>
	        <div class="cell colspan3">
	            <div class="input-control select full-size" style="position: absolute;margin-top: -4px">
					<select name="code_equipe[]" multiple STYLE="height:100px">
					<?php
					$req_cpc=mysqli_query($idBase,'SELECT distinct Equipe FROM `e_equipes` order by Equipe');
					while ($res_cpc=mysqli_fetch_object($req_cpc))
					{
						$code_equipe=$res_cpc->Equipe;
						print "<option value='$code_equipe'>$code_equipe</option>";
					}
					?>
					</select>
				</div>
			</div>
		</div><br /><br />
	    <div class="row cells8">
	        <div class="cell">
	            <div class="input-control">Ann&eacute;e</div>
	        </div>
	        <div class="cell colspan3">
	            <div class="input-control select full-size" style="position: absolute;margin-top: -4px">
				<select name="code_annee[]" multiple STYLE="height:100px;width:200px">
					<?php
					$sqli="select distinct year(Date) as Annee from p_urhaqdoc,p_version p1 $conditionsbase and year(Date)!=0 order by year(Date) desc ";
					echo $sqli;
					$req_cpc=mysqli_query($idBase,$sqli);
					while ($res_cpc=mysqli_fetch_object($req_cpc))
					{
						$code_equipe=$res_cpc->Annee;
						print "<option value='$code_equipe'>$code_equipe</option>";
					}
					?>
					</select>
				</div>
			</div>
		</div>
<br /><input type="submit" value="Filtrer"><br />
	</div>
</div>
</form><br>
<?php
$sql_mess2=preg_replace("/like '%/","like '!",$sql_mess);
// echo $sql_mess;
if( (isset($_GET['code_annee']) && !empty($_GET['code_annee'])) || (isset($_GET['code_equipe']) && !empty($_GET['code_equipe'])))
{
	$nb=0;
	if ($total>0)
	{
		$tab=array();
		$tab2=array();
		$tab_tot=array();
		$tab_ae=array();
		while($donnees_messages=mysqli_fetch_assoc($retour_messages)) // On lit les entr�es une � une gr�ce � une boucle
		{
				$Annee=$donnees_messages["Annee"];
				$Equipe=$donnees_messages["Equipe"];
				$Document=$donnees_messages["Document"];
				$ae="$Annee!$Equipe";
				$total++;
				if (!isset($tab_ae[$ae]))							$tab_ae[$ae]=$ae;
				if (isset($tab_tot[$Annee][$Equipe]))				$tab_tot[$Annee][$Equipe]++;
				else 												$tab_tot[$Annee][$Equipe]=1;
				if (isset($tab2[$Annee][$Equipe]) && $Document!='')	$tab2[$Annee][$Equipe]++;
				elseif ($Document!='')								$tab2[$Annee][$Equipe]=1;
		}

		ksort($tab_ae);
		echo '<table class="table striped hovered cell-hovered border"  id="maTable"><thead><tr><th>Annee</th><th>Equipe</th><th>Nb Total MO</th><th>MO sans Objet</th>
		<th>MO avec docs</th></tr></thead><tbody>';
		foreach ($tab_ae as $cle=>$valeur)
		{
			list($annee,$equipe)=explode("!",$valeur);
			echo "<tr><td class=td1>$annee</td><td class=td1>$equipe</td>";
			if (isset($tab_tot[$annee][$equipe]))				echo "<td>".$tab_tot[$annee][$equipe]."</td>";
			else 												echo "<td></td>";
			if (isset($tab[$annee][$equipe]["Sans Objet"]))		echo "<td>".$tab[$annee][$equipe]["Sans Objet"]."</td>";
			else 												echo "<td></td>";
			if (isset($tab2[$annee][$equipe]))					echo "<td>".$tab2[$annee][$equipe]."</td>";
			else 												echo "<td></td>";
			echo "</tr>";
		}
		echo "</tbody></table>";
	}
	?>
	<br /><br />
	<?php
	// $filtre=1;
	// if ($filtre==1)
	// {
	// 	include("./FusionCharts/FusionCharts.php");
	// 	echo "<center><table border=0><tr>";
	// 	$nb=0;

	// 	foreach($tab as $cle=>$valeur)
	// 	{
	// 		$sql= "$list_attr where Annee='$cle' $group";
	// 		$req = mysqli_query($idBase,$sql);

	// 		$strXML = "<chart labelDisplay='Rotate' slantLabels='1' formatNumber='0' formatNumberScale='0' caption='Situation pour Equipe:".$cle." - Annee:".$_GET['Annee']."' xAxisName='Valide?' canvasBgAlpha='30' yAxisName='Total' showValues='1' 
	// 		alternateHGridColor='FCB541' alternateHGridAlpha='20' divLineColor='FCB541' divLineAlpha='50' canvasBorderColor='666666' baseFontColor='666666' lineColor='FCB541'>";
	// 		$total=0;
	// 		$res=mysqli_fetch_object($req);
	// 		foreach ($entete as $key => $valeur)
	// 		{
	// 			switch ($key)
	// 			{
	// 				case "Non":
	// 					$color="0000FF";
	// 					break;
	// 				case "Oui":
	// 					$color="FF0000";
	// 					break;
	// 				default:
	// 					$color="00FF00";
	// 					break;
	// 			}
	// 			if (isset($tab[$cle][$valeur]))	
	// 			{
	// 				$value=round($tab[$cle][$valeur],2);
	// 				$strXML .= "<set color='".$color."' label='".$key."' value='".$value."' />";
	// 				$total+=$value;
	// 			}
	// 			else $strXML .= "<set color='".$color."' label='".$key."' value='0' />";
	// 		}
	// 		$strXML .= "</chart>";
	// 		if ($nb%2==0)	echo "</tr><tr>";
	// 		echo "<td>".renderChartHTML("./FusionCharts/Column3D.swf", "", $strXML, "myNext", 600, 400, false);
	// 		echo "</td>";
	// 		$nb++;
	// 	}
	// 	echo "</tr></table></center>";
	// }
}
?>