<?php
$list_attr="SELECT year(Date) as Annee,Equipe,Valide,count(*) as Nb from p_urhaqdoc,p_version p1 ";
$conditions=" where (Forme = 'Mode op�ratoire' and Obsolete='Non' and (p1.IDversion>=ALL(select IDversion from p_version p2 where p1.IDdoc=p2.IDdoc) or p1.DateAjout is NULL) and p1.IDdoc=p_urhaqdoc.Id) and Document!='' ";
$conditionsbase=$conditions;
$group=" group by Valide,year(Date),Equipe ORDER BY year(Date),Valide";

$filtre=0;
if (isset($_GET["conditions"])) $conditions=$_GET["conditions"];

if(isset($_GET['Annee'])) 
{
	if ($_GET['Annee']!="All")	{$conditions.=" and year(Date) ='".$_GET['Annee']."'";$filtre=1;}
}
if(isset($_GET['Equipe'])) 
{
	if ($_GET['Equipe']!="All")	{$conditions.=" and Equipe ='".$_GET['Equipe']."'";$filtre=1;}
}
// requete
$sql_mess="$list_attr $conditions $group";
print $sql_mess;
$retour_messages=mysqli_query($idBase,$sql_mess);
$total=mysqli_num_rows($retour_messages);

?>

<table id="tab_filtre">
<tr>
	<td>
		<form action="./index.php" method="get" name="recherche_gauche">
		<input type="hidden" name="exploration" value="5">
		<fieldset style="width:730px;">
			<legend>Filtre</legend>
			<table id="tab_filtre">
				<tr><td width=33%>Ann&eacute;e</td>
				<td><select name="Annee">
				<?php
				$sqli="select distinct year(Date) as Annee from p_urhaqdoc,p_version p1 $conditionsbase and year(Date)!=0 order by year(Date) desc ";
				echo $sqli;
				$req_equipe=mysqli_query($idBase,$sqli);
				while ($res_equipe=mysqli_fetch_object($req_equipe))
				{
					$Annee=$res_equipe->Annee;
					if ($Annee == "") $Annee="(NULL)";
					else print "<option value=$Annee>$Annee</option>";
				}
				?>
				</select></td>
			</tr><tr>
				<td width=33%>Equipe</td>
				<td><select name="Equipe"><option value="All">Toutes</option>
				<?php
				$sqli="select distinct Equipe from p_urhaqdoc,p_version p1 $conditionsbase and year(Date)!=0 order by Equipe";
				echo $sqli;
				$req_equipe=mysqli_query($idBase,$sqli);
				while ($res_equipe=mysqli_fetch_object($req_equipe))
				{
					$Equipe=$res_equipe->Equipe;
					if ($Equipe == "") $Equipe="(NULL)";
					else print "<option value=$Equipe>$Equipe</option>";
				}
				?>
				</select></td></tr>
				<tr><td colspan=4><input type="submit" value="rechercher"></td></tr>
			</table>
		</fieldset>
		</form>
	</td>
</tr>
</table><br>
<?php
$sql_mess2=preg_replace("/like '%/","like '!",$sql_mess);
// echo $sql_mess;
if (isset($_GET["Annee"]))
{
	$nb=0;
	if ($total>0)
	{
		$tab=array();
		$entete=array();
		while($donnees_messages=mysqli_fetch_assoc($retour_messages)) // On lit les entr�es une � une gr�ce � une boucle
		{
				$Equipe=$donnees_messages["Equipe"];
				$Valide=$donnees_messages["Valide"];
				if (!isset($entete[$Valide]))	$entete[$Valide]=$Valide;
				$total=$donnees_messages["Nb"];
				$tab[$Equipe][$Valide]=$total;
		}
		echo '<table id=tab border="1" align="center" cellpadding="2" cellspacing="2" style="font-size:12px;"><tr class="th1"><th>Annee</th>';
		foreach ($entete as $cle => $valeur)	
		{
			echo "<th>$valeur</th>";
		}
		echo "<th>Total</th></tr>";
		$lignetotal=array();
		foreach($tab as $cle=>$valeur)
		{
			if ($cle=="0")	$cle2="Non renseign&eacute;";
			else 			$cle2=$cle;
			echo "<tr><td class=td1>$cle2</td>";
			$total=0;
			foreach ($entete as $key => $valeur)
			{

				if (isset($tab[$cle][$valeur]))	
				{
					echo "<td style='text-align:right;width:100px;'>".$tab[$cle][$valeur]."</td>";
					$total+=$tab[$cle][$valeur];
					if (isset($lignetotal[$key]))	$lignetotal[$key]+=$tab[$cle][$valeur];
					else 							$lignetotal[$key]=$tab[$cle][$valeur];
				}
				else 							echo "<td></td>";
			}
			echo "<td class=td2 style='text-align:right;width:100px;'>".$total."</td></tr>";
		}
		echo "<tr><td class=td1>Total</td>";
		$total=0;
		foreach ($lignetotal as $key => $valeur)		
		{
			echo "<td class=td2 style='text-align:right;width:100px;'>".$total."</td>";
			$total+=$valeur;
		}
			echo "<td class=td3 style='text-align:right;width:100px;'>".$total."</td>";
		echo "</table>";
	}
	?>
	<br /><br />
	<?php
	$filtre=1;
	if ($filtre==1)
	{
		include("./FusionCharts/FusionCharts.php");
		echo "<center><table border=0><tr>";
		$nb=0;

		foreach($tab as $cle=>$valeur)
		{
			$sql= "$list_attr where Annee='$cle' $group";
			$req = mysqli_query($idBase,$sql);

			$strXML = "<chart labelDisplay='Rotate' slantLabels='1' formatNumber='0' formatNumberScale='0' caption='Situation pour Equipe:".$cle." - Annee:".$_GET['Annee']."' xAxisName='Valide?' canvasBgAlpha='30' yAxisName='Total' showValues='1' 
			alternateHGridColor='FCB541' alternateHGridAlpha='20' divLineColor='FCB541' divLineAlpha='50' canvasBorderColor='666666' baseFontColor='666666' lineColor='FCB541'>";
			$total=0;
			$res=mysqli_fetch_object($req);
			foreach ($entete as $key => $valeur)
			{
				switch ($key)
				{
					case "Non":
						$color="0000FF";
						break;
					case "Oui":
						$color="FF0000";
						break;
					default:
						$color="00FF00";
						break;
				}
				if (isset($tab[$cle][$valeur]))	
				{
					$value=round($tab[$cle][$valeur],2);
					$strXML .= "<set color='".$color."' label='".$key."' value='".$value."' />";
					$total+=$value;
				}
				else $strXML .= "<set color='".$color."' label='".$key."' value='0' />";
			}
			$strXML .= "</chart>";
			if ($nb%2==0)	echo "</tr><tr>";
			echo "<td>".renderChartHTML("./FusionCharts/Column3D.swf", "", $strXML, "myNext", 600, 400, false);
			echo "</td>";
			$nb++;
		}
		echo "</tr></table></center>";
	}
}
?>