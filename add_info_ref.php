<?php
$width="";
// on recupere le nom de la table
if(isset($_GET["nomtable"]))
	$nom_table=$_GET["nomtable"];
// on recupere la cle primaire de la table
$cle=$_GET["cle"];

// si on est en mode "ajout"
if (($_GET["modif"])=="ajout")
{
	echo "<h2>Ajout d'un enregistrement =</h2>";
	// on recupere les champs de la table voulue
	$result = mysqli_query($idBase,"SHOW COLUMNS FROM `$nom_table`");
	$nb_fields=mysqli_num_rows($result);
	$width=100*$nb_fields;
	?>
	<form action="./index.php" method="post" enctype="multipart/form-data">
	    <input type=hidden name=infos value="on">
		<input type="hidden" name="infos" value="referentiel">
	    <input type=hidden name=validmodifref value="ajouter">
	    <input type=hidden name=cle value="<?php echo $cle;?>">
	    <input type=hidden name=nomtable value="<?php echo $nom_table;?>">
	    <div class="example" data-text="insertion">
			<div class="grid">
	<?php
	// pour chaque element de la table
	while ($row = mysqli_fetch_object($result)) 
	{
		// si ce n'est pas la cle primaire (auto-increment)
	    if ($row->Field <> $cle)
	    {
			// on affiche la ligne correspondant
	        setInput($row->Field,"");
	    }
	}

?>
<br /><input type="submit" value="Ajouter"><br />
		 	</div>
		 </div>
	</form>
<?php
}
// si on est en mode "modification"
elseif (($_GET["modif"])=="update")
{
	echo "<h2>Modification de l'enregistrement</h2>";
	$val=$_GET["num"];
	$result = mysqli_query($idBase,"SHOW COLUMNS FROM `$nom_table`");
	$nb_fields=mysqli_num_rows($result);
	$width=100*$nb_fields;
	?>
	<form action="./index.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="infos" value="referentiel">
	    <input type=hidden name=infos value="on">
	    <input type=hidden name=validmodifref value="update">
	    <input type=hidden name=cle value="<?php echo $cle;?>">
	    <input type=hidden name=val value="<?php echo $val;?>">
	    <input type=hidden name=nomtable value="<?php echo $nom_table;?>">
	    <div class="example" data-text="insertion">
			<div class="grid">
	<?php
	// idem ajout, sauf que pour chaque element, on affiche la valeur actuelle
	while ($row = mysqli_fetch_object($result)) 
	{
	    if ($row->Field <> $cle)
	    {
	    	$sql="select ".$row->Field." from `$nom_table` where $cle=$val"; 
	    	$req=mysqli_query($idBase,$sql);
	    	$res=mysqli_fetch_object($req);
			$row_field=$row->Field;
			setInput($row_field,$res->$row_field);
	    }
	}


?>
<br /><input type="submit" value="Modifier"><br />
		 	</div>
		 </div>
	</form>
<?php
}

function setInput($Field,$resultat)
{
	global $td_width,$nom_table;
	switch ($Field) {
	default:
		createInputText($Field,$Field,$resultat);
		break;
	}
}
?>