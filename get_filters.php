<?php
		// identifiant
		get_text_filtre("$id_attr");

	// REGLEMENTATION
		// t_elevage_ele
		get_text_filtre("ele_titre");
		// t_ethiqueexpeanimale_eea
		get_text_filtre("eea_titre");
		// t_agrements_agr 
		get_text_filtre("agr_titre");
		get_multi_filtre("agr_site");

	// SBEA
		// t_groupesbea_grs 
		get_text_filtre("grs_nomprenom");
		get_multi_filtre("grs_site");
		// t_crreunionssbea_crs  
		get_text_filtre("crs_intitule");
		get_multi_filtre("crs_site");
		// t_docrefreth_dre 
		get_text_filtre("dre_titre");

	// CHARTE SANITAIRE
		// t_groupecs_gcs 
		get_text_filtre("gcs_nomprenom");
		get_multi_filtre("gcs_site");
		// t_suivics_tcs  
		get_text_filtre("tcs_intitule");
		get_multi_filtre("tcs_site");
		// t_planactioncs_pls   
		get_text_filtre("pls_intitule");
		get_multi_filtre("pls_site");
		// t_crreunionscs_ccs 
		get_text_filtre("ccs_intitule");
		// t_documentationcs_dcs  
		get_text_filtre("dcs_titre");

	// DOCUMENTATION AQ
		// t_documentation_doi
		get_text_filtre("doi_titre");
		get_multi_filtre("doi_equipe");
		get_multi_filtre("doi_forme");
		// t_documentexterne_doe
		get_text_filtre("doe_titre");
		get_multi_filtre("doe_application");
		get_multi_filtre("doe_origine");
		get_multi_filtre("doe_type");

	// RESSOURCES HUMAINES
		// t_habilitations_hab  
		get_text_filtre("hab_nomprenom");
		get_multi_filtre("hab_equipe");
		get_multi_filtre("hab_habilitation");
		// t_feea_fee   
		get_text_filtre("fee_nomprenom");
		get_multi_filtre("fee_site");
		get_text_filtre("fee_titre");
		// t_formationaq_faq    
		get_text_filtre("faq_nomprenom");
		get_multi_filtre("faq_equipe");
		get_text_filtre("fee_titre");
		// t_stagiaires_sta    
		get_text_filtre("sta_nomprenom");
		get_multi_filtre("sta_site");

	// PREVENTION
		// t_groupeprevention_grp 
		get_text_filtre("grp_nomprenom");
		get_multi_filtre("grp_site");
		// t_suiviprevention_sup  
		get_multi_filtre("sup_site");
		// t_documentationprevention_dop  
		get_text_filtre("dop_titre");

	// MANAGEMENT QUALITE
		// t_referentielqualite_req   
		get_text_filtre("req_titre");
		// t_politiqueaq_paq    
		get_text_filtre("paq_titre");
		// t_organisationaq_oaq     
		get_text_filtre("oaq_titre");
		// t_groupeaq_gaq     
		get_text_filtre("gaq_nomprenom");
		get_multi_filtre("gaq_site");
		// t_autoevalaq_aea      
		get_text_filtre("aea_intitule");
		get_multi_filtre("aea_site");
		// t_planactionaq_pla       
		get_text_filtre("pla_intitule");
		get_multi_filtre("pla_site");
		// t_auditsaq_aua        
		get_text_filtre("aua_intitule");
		get_multi_filtre("aua_site");
		// t_crreunionsaq_cra         
		get_text_filtre("cra_intitule");

	// MATERIEL
		// t_materiel_mat
		get_multi_filtre("mat_site");
		get_multi_filtre("mat_equipe");
		get_multi_filtre("mat_nature");
		get_multi_filtre("mat_marque");

	// EXPERIMENTATIONS
		// t_protocoles_pro      
		get_text_filtre("pro_titre");
		get_multi_filtre("pro_site");
		get_multi_filtre("pro_equipe");
		// t_cahierlabo_cla       
		get_text_filtre("cla_intitule");
		get_multi_filtre("cla_site");
		get_multi_filtre("cla_equipe");
		// t_echantillons_ech         
		get_multi_filtre("ech_site");

	// COMMUNICATION
		// t_communicationinterne_coi        
		get_text_filtre("coi_titre");
		// t_communicationexterne_coe        
		get_text_filtre("coe_titre");
?>