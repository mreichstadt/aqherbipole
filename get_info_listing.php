<?php
	if (isset($_SESSION["conditions"])) $conditions=$_SESSION["conditions"]; else $condition="";
	$type=$_GET["info"];
	$infos=$_GET["info"];

	// en fonction de la tables selectionnee, les attributs affich�s changent, ainsi que le titre, et la condition
	include("get_info_from_table.php");

// recuperation de la condition (le where de la requete)
	if(isset($_GET["conditions"])) 	
	{
		$conditions=$_GET["conditions"];
		$conditions=preg_replace('/\\\/', '', $conditions); 
		$conditions=preg_replace("/like '!/","like '%",$conditions);
		$conditions=preg_replace("/!'/","%'",$conditions);
	}
	if (preg_match("/materiel/", $type))				$nom_table="t_materiel_mat";
	elseif (preg_match("/documentinterne/", $type))		$nom_table="t_documentinterne_doi";
	else 												$nom_table=$type;

	include("get_filters.php");
	
	// creation de la requete de selection
	$sql_mess="SELECT distinct $list_attr from $nom_table";
	$sql_mess.=" where $conditions";
	$_SESSION["conditions"]=$conditions;

	if (isset($_GET["recherche"]))
	{
?>
		<table style="width:1200px;border-style:none;">
		<!-- si on vient de faire une recherche, possibilit� de retour arriere avec l'icone retour -->
		<tr style="border-style:none;"><td style="width:60px;border-style:none;"><a id="retourrecherche" onclick="history.go(-1)" onmouseover="changeCursor('pointer','retourrecherche')" onmouseout="changeCursor('default','retourrecherche')"><img src="images/bouton_retour.png" style="border-style:none;width:50px;height:50px;"></a></td>
		<td style="width:1140px;border-style:none;"><h3><center><?php echo utf8_encode($titre);?></center></h3></td></tr></table>
<?php
	}
	else		echo "<h3><center>".utf8_encode($titre)."</center></h3>";
	// affichage de la partie filtre pour les tables correspondantes
	include("set_filters.php");
	// a mettre en place pour eviter les erreurs de parametres
	$sql_mess2=preg_replace("/like '%/","like '!",$sql_mess);

	if ($_SESSION["user"]=="uid=mreichstadt")	
	print $sql_mess."<br>";//show columns from $type";
	
	// execution de la requete de selection
	$retour_messages=mysqli_query($idBase,$sql_mess);
	$attr=mysqli_fetch_fields($retour_messages);
	$nb_enregistrements=mysqli_num_rows($retour_messages);
	$cle=$id_attr;
	$nb=0;	
?><br />
<a href="export_csv.php?type=<?php echo $type;?>&sql=<?php echo $sql_mess2;?>"><button class="button success"><span class="mif-download"></span> Export excel</button></a>
<?php
// si on est enregistr�, alors avant le 1er enregistrement on peut ajouter un enregistrement en cliquant sur le lien correspondant
	if (($_SESSION["UserLevel"]==-1)||(($_SESSION["UserLevel"]==3)&&($type!="demandes"))/*||(($_SESSION["UserLevel"]==2)&&($type=="`a_mat`"))*/) 
	{
	
	?>
			<a href="./index.php?add_info=on&modif=ajout&nomtable=<?php echo $type;?>&cle=<?php echo $cle;?>&infos_modif=<?php echo $infos;?>&type=<?php echo $type;?>&conditions=<?php echo $conditions;?>"><button class="button primary"><span class="mif-plus"></span> Ajouter</button></a><br>
	<?php
	}
	if (mysqli_num_rows($retour_messages)>0)
	{
		// puis affichage des entetes de tableaux avec les tris possibles
		echo '<table class="table striped hovered cell-hovered border"  id="maTable"><thead>
				<tr><th style="width:20px;">&nbsp;</th>';
		if ($nom_table=="t_materiel_mat")			echo  '<th style="width:20px;">&nbsp;</th><th style="width:20px;">&nbsp;</th>';
		if ($nom_table=="t_protocoles_pro")			echo  '<th style="width:20px;">&nbsp;</th>';
		if ($nom_table=="t_documentinterne_doi")	echo  '<th style="width:20px;">&nbsp;</th>';
		foreach($attr as $val)
		{
			$nomchamp=$val->name;
			if ($nom_table=="t_documentinterne_doi" && $nomchamp=="doi_commentaire")  		echo '<th>AuteursV1</th><th>AuteursNewV</th><th>Date</th><th>Document</th>';
			if (preg_match("/pro_die/", $nomchamp) || preg_match("/pro_prp/", $nomchamp))	echo '<th>'.strtoupper(substr($nomchamp,4)).'</th>';
			else 																			echo '<th>'.substr($nomchamp,4).'</th>';
		}
		echo '</tr></thead><tbody>';
		// tant qu'on a des r�sultats
		while($donnees_messages=mysqli_fetch_assoc($retour_messages)) // On lit les entr�es une � une gr�ce � une boucle
		{
			// sinon on met en place une alternance de couleurs
			echo '<tr>';
			// affichage des valeurs pour chaque champs
			$numero_champ=-1;
			foreach($attr as $val)
			{
				$numero_champ++;
				$nomchamp=$val->name;
				$infomess=$donnees_messages[$nomchamp];
				$infocle=$donnees_messages[$cle];
				if ($numero_champ==0){
					?>
					<td>
					<?php
					// pour les utilisateurs enregistr�s, possibilit� de modifier un enregistrement
					// pour l'administrateur (userlevel=-1), possibilit� de supprimer un enregistrement
					if (isset($_SESSION["user"])) {

						if (($_SESSION["UserLevel"]==-1)||($_SESSION["UserLevel"]==2)|| ($_SESSION["UserLevel"]==3 && preg_match("/prevention/",$type)))
						{	
						?>
							<a href="./index.php?modif_info=on&modif=update&cle=<?php echo $cle;?>&num=<?php echo $infocle;?>&infos_modif=<?php echo $infos;?>&nomtable=<?php echo $type;?>&type=<?php echo $type;?>&conditions=<?php echo $conditions;?>" title="modifier"><span class="mif-pencil"></span></a>
							<br><a href="./?deleteinfo=delete&nomtable=<?php echo $type;?>&cle=<?php echo $cle;?>&num=<?php echo $infocle;?>&infos_modif=<?php echo $infos;?>&type=<?php echo $type;?>&conditions=<?php echo $conditions;?>" onclick="return confirm('Voulez-vous vraiment suprimer cet enregistrement ?');" title="supprimer"><span class="mif-cross fg-red"></span></a>
							<br><a href="./index.php?modif_info=on&modif=ajouter2&cle=<?php echo $cle;?>&num=<?php echo $infocle;?>&infos_modif=<?php echo $infos;?>&nomtable=<?php echo $type;?>&type=<?php echo $type;?>&conditions=<?php echo $conditions;?>" title="ajouter depuis enregistrement"><span class="mif-plus fg-green"></span></a>
						<?php
						}
					}?>
					</td>			
					<?php
					// protocoles
					if ($nom_table=="t_protocoles_pro")	
					{
						$cle="pro_id";
						$val=$donnees_messages[$cle];
						$req_det=mysqli_query($idBase,"select count(*) nb from td_versionprotocole_vep where $cle=".$val);
						$res_det=mysqli_fetch_object($req_det);
						$nb_det=$res_det->nb;
						 ?>
						<td><a href="./index.php?detail=on&nomtableref=<?php echo $type;?>&nomtable=td_versionprotocole_vep&cleref=<?php echo $cle;?>&cle=<?php echo $cle;?>&val=<?php echo $val;?>" target="_blank">Version(<?php echo $nb_det;?>)</a></td>
						<?php
					}	
					// doc interne
					if ($nom_table=="t_documentinterne_doi")	
					{
						$cle="doi_id";
						$val=$donnees_messages[$cle];
						$req_det=mysqli_query($idBase,"select count(*) nb from td_versiondocinterne_vdi where $cle=".$val);
						$res_det=mysqli_fetch_object($req_det);
						$nb_det=$res_det->nb;
						 ?>
						<td><a href="./index.php?detail=on&nomtableref=<?php echo $type;?>&nomtable=td_versiondocinterne_vdi&cleref=<?php echo $cle;?>&cle=<?php echo $cle;?>&val=<?php echo $val;?>" target="_blank">Version(<?php echo $nb_det;?>)</a></td>
						<?php
					}	
					// materiel
					if ($nom_table=="t_materiel_mat")	
					{
						$cle="mar_id";
						$val=$donnees_messages[$cle];
						// detail
						$req_det=mysqli_query($idBase,"select count(*) nb from td_materieldetail_mad where mar_id=".$val);
						$res_det=mysqli_fetch_object($req_det);
						$nb_det=$res_det->nb;
						 ?>
						<td><a href="./index.php?detail=on&nomtableref=<?php echo $nom_table;?>&nomtable=td_materieldetail_mad&cleref=<?php echo $cle;?>&cle=mar_id&val=<?php echo $val;?>" target="_blank">Detail(<?php echo $nb_det;?>)</a></td>
						<?php
						// historique
						$req_det=mysqli_query($idBase,"select count(*) nb from td_materielhistorique_mah where mar_id=".$val);
						$res_det=mysqli_fetch_object($req_det);
						$nb_det=$res_det->nb;
						 ?>
						<td><a href="./index.php?detail=on&nomtableref=<?php echo $nom_table;?>&nomtable=td_materielhistorique_mah&cleref=<?php echo $cle;?>&cle=mar_id&val=<?php echo $val;?>" target="_blank">Histo(<?php echo $nb_det;?>)</a></td>
						<?php
					}		
				}
				// si nous avons un champ cliquable (document � t�l�charger)

				if (in_array($nomchamp, $tab_document))
				{
					if (!isset($_SESSION["UserLevel"])) 			echo "<td>Fichier</td>";
					elseif ($donnees_messages[$nomchamp] != "")		echo '<td><a href="./uploads/'.$nom_table.'/'.$donnees_messages[$nomchamp].'" target="blank">Fichier</a></td>';
					else											echo '<td>'.$donnees_messages[$nomchamp].'</td>';
				}
				elseif (in_array($nomchamp, $tab_photo) && preg_match("/materiel/", $nom_table))
				{
					if ((file_exists("./uploads/materiels/".$donnees_messages[$nomchamp]))&& ($donnees_messages[$nomchamp]!=""))
					{
						if ((filesize("./uploads/materiels/".$donnees_messages[$nomchamp])) > 0) 	echo "<td><img src='./uploads/materiels/".$donnees_messages[$nomchamp]."' width=100></td>";
						else	echo '<td></td>';//<font style="font-size:8px;">'.$nomprenom2.'</font>
					}
					else		echo '<td></td>';//<font style="font-size:8px;">'.$nomprenom2.'</font>
				}
				elseif (in_array($nomchamp, $tab_photo))
				{
					if ((file_exists("./uploads/$nom_table/".$donnees_messages[$nomchamp]))&& ($donnees_messages[$nomchamp]!=""))
					{
						if ((filesize("./uploads/$nom_table/".$donnees_messages[$nomchamp])) > 0) 	echo "<td><img src='./uploads/$nom_table/".$donnees_messages[$nomchamp]."' width=100></td>";
						else	echo '<td></td>';//<font style="font-size:8px;">'.$nomprenom2.'</font>
					}
					else		echo '<td></td>';//<font style="font-size:8px;">'.$nomprenom2.'</font>
				}
				elseif (in_array($nomchamp, $tab_site))
				{
					$infoTab=substr($nomchamp,4);
					$sql_eq="select ".$tabRef[$infoTab]["libelle"]." as libelle from ".$tabRef[$infoTab]["table"]." where ".$tabRef[$infoTab]["id"]."=".$donnees_messages[$nomchamp];
					$req_eq=mysqli_query($idBase,$sql_eq);
					if (mysqli_num_rows($req_eq)>0)
					{
						$res_eq=mysqli_fetch_object($req_eq);
						echo "<td>".$res_eq->libelle."</td>";
					}
					else echo "<td>".$donnees_messages[$nomchamp]."</td>";;
				}
				elseif (in_array($nomchamp, $tab_radio))
				{
					if ($donnees_messages[$nomchamp]==0)	echo "<td></td>";
					if ($donnees_messages[$nomchamp]==1)	echo "<td>oui</td>";
					if ($donnees_messages[$nomchamp]==2)	echo "<td>non</td>";
					if ($donnees_messages[$nomchamp]==3)	echo "<td>?</td>";
				}

				// affichage des dates au format jj/mm/aaaa
				elseif (in_array($nomchamp,$tab_date))
				{
					$date="";
					if ($donnees_messages[$nomchamp] != "")
					{
						list($a,$m,$j)=explode('-',$donnees_messages[$nomchamp]);
						$date="$j/$m/$a";
					}
					echo '<td>'.$date.'</td>';
				}
				// si on est dans le cas des commentaires sur les docs internes on met les infos de la table version
				elseif($nomchamp=="doi_commentaire")
				{
					$req_version=mysqli_query($idBase,"select * from td_versiondocinterne_vdi where doi_id=".$donnees_messages["doi_id"]." order by vdi_nversion desc limit 0,1");
					if (mysqli_num_rows($req_version)==0) echo "<td></td><td></td><td></td><td></td>";
					else
					{
						$req_version2=mysqli_query($idBase,"select * from td_versiondocinterne_vdi where doi_id=".$donnees_messages["doi_id"]." order by vdi_nversion limit 0,1");
						$res_version2=mysqli_fetch_object($req_version2);
						$res_version=mysqli_fetch_object($req_version);
						echo "<td>".$res_version2->vdi_auteurs."</td><td>".$res_version->vdi_auteurs."</td><td>".set_format($res_version->vdi_date)."</td>";
						if (!isset($_SESSION["UserLevel"])) 			echo "<td>Fichier</td>";
						elseif ($res_version->vdi_document != "")		echo '<td><a href="https://www1.clermont.inra.fr/appurh/AQHerbipole/uploads/td_versiondocinterne_vdi/'.$res_version->vdi_document.'" target="blank">Fichier</a></td>';
						else											echo '<td>'.$res_version->vdi_document.'</td>';
						mysqli_free_result($req_version2);
					}
					mysqli_free_result($req_version);
					echo '<td>'.$donnees_messages[$nomchamp].'</td>';				
				}
				else 	echo '<td>'.$donnees_messages[$nomchamp].'</td>';
			}

			print "</tr>";
		}
		?>
		</tbody></table><br />
<?php
	}
	else echo "Aucun r&eacute;sultat";
?>
<script>
$(document).ready(function() {
    var table = $('#maTable').DataTable( {
      columnDefs: [
          { width: 50, targets: 0 }
      ],
      fixedColumns: true,
      pagingType: "full_numbers",
      language: {
        "lengthMenu": "_MENU_ enregistrements par page",
        "search": "Rechercher",
        "zeroRecords": "aucun r�sultat",
        "info": "_MAX_ enregistrements",
        "infoEmpty": "Pas de r�sultats",
        "infoFiltered": "(filtered from _MAX_ total records)",
      },
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Toutes"]],
      dom: '<"top"Bli>rt<"bottom"p><"clear">,',
      fixedHeader: {
        headerOffset: 50
      },

    } );

});
</script>