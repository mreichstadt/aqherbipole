
<?php
if(isset($_GET["nomtable"]))
	$nom_table=$_GET["nomtable"];
else
	$nom_table="_equipe";
$retour_messages=mysqli_query($idBase,"SELECT * FROM $nom_table");
$attr=mysqli_fetch_fields($retour_messages);
// inclusion de la pagination
$result = mysqli_query($idBase,"SHOW COLUMNS FROM `$nom_table`");
// on recupere le nom de la cl� primaire
while ($row = mysqli_fetch_object($result)) 
{
	if ($row->Key == "PRI") {$cle=$row->Field;}
}
// on selectionne les attributs de la table
$result = mysqli_query($idBase,"SHOW COLUMNS FROM $nom_table");
$nb_fields=mysqli_num_rows($result);
?>
<a href="./index.php?add_info_ref=on&modif=ajout&nomtable=<?php echo $nom_table;?>&cle=<?php echo $cle;?>"><button class="button primary"><span class="mif-plus"></span> Ajouter</button></a><br>
<table class="table striped hovered cell-hovered border"  id="maTable">
	<thead>
        <tr><th></th>
<?php
foreach ($attr as $val)	echo "<th>".$val->name."</th>";
echo '</tr></thead><tbody>';
// pour chaque enregistrement
while($donnees_messages=mysqli_fetch_assoc($retour_messages)) // On lit les entr�es une � une gr�ce � une boucle
{
	$infocle=$donnees_messages[$cle];
	echo '<tr>';
	?><td><a href="./index.php?modif_info_ref=on&modif=update&cle=<?php echo $cle;?>&num=<?php echo $infocle;?>&nomtable=<?php echo $nom_table;?>"><span class="mif-pencil"></span></a>
	<?php 
		if ($_SESSION["UserLevel"]==-1)
	?>
		<br><a href="./?deleteinforef=delete&nomtable=<?php echo $nom_table;?>&cle=<?php echo $cle;?>&num=<?php echo $infocle;?>" onclick="return confirm('Voulez-vous vraiment suprimer cet enregistrement ?');"><span class="mif-cross fg-red"></span></a>
		</td>
	<?php
	// on recupere les informations de chaque attribut
	foreach ($attr as $val)	 
	{
		$nomchamp=$val->name;
		// si c'est un lien on l'affiche
		if ($nomchamp == "Lien") 	echo '<td><a href="'.$donnees_messages[$nomchamp].'" target="_blank">'.$donnees_messages[$nomchamp].'</a></td>';
		else						echo '<td>'.$donnees_messages[$nomchamp].'</td>';
	}
    echo '</tr>';
}
?>
</tbody></table><br /><br />
<script>
$(document).ready(function() {
    var table = $('#maTable').DataTable( {
      columnDefs: [
          { width: 50, targets: 0 }
      ],
      fixedColumns: true,
      pagingType: "full_numbers",
      language: {
        "lengthMenu": "_MENU_ enregistrements par page",
        "search": "Rechercher",
        "zeroRecords": "aucun r�sultat",
        "info": "_MAX_ enregistrements",
        "infoEmpty": "Pas de r�sultats",
        "infoFiltered": "(filtered from _MAX_ total records)",
      },
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Toutes"]],
      dom: '<"top"Bli>rt<"bottom"p><"clear">,',
      fixedHeader: {
        headerOffset: 50
      },

    } );

});
</script>